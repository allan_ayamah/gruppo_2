package client;

import java.io.IOException;
import java.rmi.Naming;

import client.ui.DataInput;
import client.ui.DialogWelcome;
import client.ui.GameUIConstants;
import client.ui.board.GameBoard;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import shared.bean.SessionBean;
import shared.service.GameService;

public class Client extends Application {
	public Stage mainStage, wStage;

	private GameBoard board;
	private DialogWelcome diaWelcome;

	private GameService gameService;
	private GameClient gameClient;
	
	public void initClient(final Stage stage) throws IOException {
		mainStage = stage;
		// BoardConfig conf = new BoardConfigImpl(new DefaultBoardBuilder());
		// System.out.println("b");

		try {
			getConnection();
		} catch (Exception e1) {
			System.out.println("Couldn't connect to server");
			e1.printStackTrace();
		}

		// SessionBean s1D = gameService.createSession(p1);

		initWaiting();

		diaWelcome = new DialogWelcome(wStage, true);
		diaWelcome.setTitle("Welcome");
		diaWelcome.initDialog();

		diaWelcome.getBtnCreate().setOnAction(e -> initNewSession());
		diaWelcome.getBtnJoin().setOnAction(e -> getJoinSesssionDialog());

		// for(int i=1; i<5;i++){
		// if(i==1){
		// gameClient = new GameClient("Allan", mainStage);
		// SessionBean bean = gameService.createSession(gameClient);
		// gameClient.setSession(bean.getGameSession());
		// }
		// }
		// gameClient.initGraphics(bean.getGameSession().getBoardConfiguration());
		// System.err.println("after
		// gra"+bean.getGameSession().getBoardConfiguration().getSpaces().size());
		diaWelcome.showAndWait();
		// System.out.println("Waiting");
		
		// System.out.println("sdf");
		// mainStage.show();

	}

	private void initWaiting() {
		wStage = new Stage();

		Pane waitingRoot;
		try {
			waitingRoot = (Pane) FXMLLoader.load(getClass().getResource("Wating.fxml"));
			Scene waitingScene = new Scene(waitingRoot, 500, 500);

			wStage.setScene(waitingScene);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void initNewSession() {
		Label lblSessionName = new Label("Session name: ");
		Label lblNickname = new Label("Nickname: ");
		TextField txtSessionName = new TextField();
		TextField txtNickName = new TextField();

		DataInputDialog diaNewSession = new DataInputDialog(diaWelcome, "Create Session", "Cancel");
		diaNewSession.addDataInput(new DataInput(lblSessionName, txtSessionName));
		diaNewSession.addDataInput(new DataInput(lblNickname, txtNickName));

		diaNewSession.getBtnOk().setOnAction(e -> {

			String sessionName = diaNewSession.getData().get(0).getTxtData().getText();
			String playerNickname = diaNewSession.getData().get(1).getTxtData().getText();
			try {

				gameClient = new GameClient(playerNickname, mainStage);
				SessionBean bean = gameService.createSession(gameClient);
				gameClient.setSession(bean.getGameSession());
				gameClient.setWaitin(wStage);
				wStage.showAndWait();
				Platform.runLater(() -> {

					try {
						gameClient.getSession().startGame();
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				});
				diaNewSession.close();
				diaWelcome.close();
			} catch (Exception e1) {
				System.out.println("Couldn't create client");
				e1.printStackTrace();
			}

		});

		diaNewSession.initDialog();
		diaNewSession.getScene().getStylesheets().add(GameUIConstants.MASTER_CSS);
		diaNewSession.show();
	}

	public void getJoinSesssionDialog() {
		Label lblSessionID = new Label("Session ID: ");
		Label lblNickname = new Label("Nickname: ");
		TextField txtSessionID = new TextField();
		TextField txtNickName = new TextField();

		DataInputDialog diaJoinSession = new DataInputDialog(diaWelcome, "Join session", "Cancel");
		diaJoinSession.addDataInput(new DataInput(lblSessionID, txtSessionID));
		diaJoinSession.addDataInput(new DataInput(lblNickname, txtNickName));
 
		diaJoinSession.getBtnOk().setOnAction(e -> {

			String sessionId = diaJoinSession.getData().get(0).getTxtData().getText();
			String playerNickname = diaJoinSession.getData().get(1).getTxtData().getText();
			try {

				gameClient = new GameClient(playerNickname, mainStage);
				SessionBean bean = gameService.joinSession(sessionId, gameClient);
				gameClient.setSession(bean.getGameSession());
				gameClient.setWaitin(wStage);
				wStage.show();
//				Platform.runLater(() -> {
//
//					try {
//						gameClient.getSession().startGame();
//					} catch (Exception e1) {
//						// TODO Auto-generated catch block
//						e1.printStackTrace();
//					}
//				});
				diaJoinSession.close();
				diaWelcome.close();
			} catch (Exception e1) {
				System.out.println("Couldn't create client");
				e1.printStackTrace();
			}

		});

		diaJoinSession.initDialog();

		diaJoinSession.getScene().getStylesheets().add(GameUIConstants.MASTER_CSS);
		diaJoinSession.show();
	}

	@Override
	public void start(Stage primaryStage) throws IOException {
		Font.loadFont("assets/Kabob.ttf", 20);
		initClient(primaryStage);
	}

	public void getConnection() throws Exception {
		String serviceName = "GameService";
		gameService = (GameService) Naming.lookup(serviceName);

	}

	public static void main(String[] args) {
		launch(args);
	}

}
