package client;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import client.ui.board.UiToken;
import client.ui.board.space.PanePos;
import javafx.scene.paint.Color;

public class UiHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final Color[] COLORS = { Color.YELLOW, Color.PINK, Color.BROWN, Color.GREEN, Color.ORANGE, Color.BLUE,Color.PURPLE, Color.RED };
	
	public static final String[] COLOR_NAMES = { "YELLOW", "PINK", "BROWN", "GREEN", "ORANGE", "BLUE","PURPLE", "RED" };
	public static final Map<Integer,PanePos>  DEF_POS = createMap();
	
	private static Map<Integer,PanePos> createMap(){
		Map<Integer,PanePos> pos = new HashMap<Integer, PanePos>();
		pos.put(0, new PanePos(0, 0));
		pos.put(0, new PanePos(0,0));
		pos.put(2, new PanePos(0,1));
		pos.put(3, new PanePos(0,2));
		pos.put(4, new PanePos(1,0)); 
		pos.put(5, new PanePos(1,1));
		pos.put(6, new PanePos(1,2));
		pos.put(7, new PanePos(2,0));
		pos.put(1, new PanePos(2,1));
		
		return pos;
		
	}
	


	public static final UiToken getTokenByCode(int code) {
		UiToken tok = new UiToken(UiHelper.COLORS[code].brighter());
		tok.setPosition(DEF_POS.get(code));
		return tok;
	}

}
