package client.ui;

import javafx.beans.binding.DoubleBinding;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class DialogWelcome extends Dialog {

	private Pane grdContainer;

	private Button btnCreate, btnJoin;

	public Button getBtnCreate() {
		return btnCreate;
	}

	public void setBtnCreate(Button btnCreate) {
		this.btnCreate = btnCreate;
	}

	public Button getBtnJoin() {
		return btnJoin;
	}

	private ImageView imgView;
	private StackPane imgPane;
	private HBox hButtons;

	public DialogWelcome(Stage parent, boolean modal) {
		super(parent, modal);

		imgPane = new StackPane();
		imgView = new ImageView(GameUIConstants.WELCOME_IMAGE);
		btnCreate = new Button("New Session");
		
		btnJoin = new Button("Join Session");
		
		hButtons = new HBox();
		grdContainer = new Pane();
		grdContainer.getStyleClass().add("welcome");
		
		
		scene = new Scene(grdContainer,1200,600);
		scene.getStylesheets().add(GameUIConstants.MASTER_CSS);
		setScene(scene);
		
	}

	public void initDialog() {
		final ColumnConstraints colCon = new ColumnConstraints();
		colCon.setPercentWidth(100);
		
		grdContainer.getChildren().add(imgPane);

		final DoubleBinding multipliedHeight = grdContainer.heightProperty().multiply(1);
		final DoubleBinding multipliedWidth = grdContainer.widthProperty().multiply(1);
		
		imgPane.maxHeightProperty().bind(multipliedHeight);
		imgPane.maxWidthProperty().bind(multipliedWidth);
		imgPane.minHeightProperty().bind(multipliedHeight);
		imgPane.minWidthProperty().bind(multipliedWidth);
		imgPane.prefHeightProperty().bind(multipliedHeight);
		imgPane.prefWidthProperty().bind(multipliedWidth);
		
		imgView.setPreserveRatio(true);
		imgView.fitWidthProperty().bind(imgPane.widthProperty());
		imgView.fitHeightProperty().bind(imgPane.heightProperty());
		imgPane.getChildren().add(imgView);
		
		hButtons.getStyleClass().add("h-buttons");
		hButtons.setSpacing(30);
		hButtons.setAlignment(Pos.TOP_CENTER);
		
		hButtons.getChildren().addAll(btnCreate, btnJoin);

		imgPane.getChildren().add(hButtons);
	}

	public ImageView getImage() {
		return imgView;
	}

	public Pane getRoot() {
		return grdContainer;
	}

	public void setRootId(String id) {
		getRoot().setId(id);
	}

}
