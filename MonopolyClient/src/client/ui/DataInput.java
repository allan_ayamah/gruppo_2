package client.ui;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class DataInput{
	
	private Label lblData;
	private TextField txtData;
	
	public DataInput( Label lbl, TextField txt) {
		lblData = lbl;
		txtData = txt;
		
		getTxtData().setMinHeight(35);
		getTxtData().setPrefHeight(35);
	}
	
	public Label getLblData() {
		return lblData;
	}

	public TextField getTxtData() {
		return txtData;
	}
}