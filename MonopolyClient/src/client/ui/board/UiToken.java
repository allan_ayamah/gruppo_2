package client.ui.board;

import client.ui.board.space.PanePos;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import shared.board.PropertyColorGroup;

public class UiToken extends Circle{

	private int currentSpace;
	private PanePos pos;
	private PropertyColorGroup color;

	public UiToken(Color color) {
		super(10.f);
		setFill(color);
		this.currentSpace = 0;
	}
	
	public void setCurrentSpace(int curr) {
		currentSpace = curr;
	}

	public int getCurrentSpace() {
		return currentSpace;
	}

	public PanePos getPosition() {
		return pos;
	}

	public void setPosition(PanePos pos) {
		this.pos = pos;
	}

}