package client.ui.board;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import client.ui.Dialog;
import client.ui.GameUIConstants;
import client.ui.board.space.UISpace;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import shared.board.BoardConfig;
import shared.board.Card;
import shared.board.PropertySpace;
import shared.board.Space;
import shared.service.GameSession;

public class GameBoard extends GridPane {
	static final int C = 11, R = 11;

	private BoardConfig bConfig;
	private GameControl controls;
	private ArrayList<UISpace> spaces;
	private Map<Integer, UiToken> tokens;

	private Text spaceDetails;

	private int cont;

	public GameBoard(BoardConfig conf, GameControl controls) {

		setBoardConfig(conf);
		tokens = new HashMap<Integer, UiToken>(GameSession.MAX_PLAYERS);
		spaces = new ArrayList<UISpace>(BoardConfig.MAX_SPACES);

		this.setId("board");
		getStyleClass().add("board");
		layoutNodes();

		populate();
		controls = controls;
		controls.setId("game-ctrls");
		StackPane cntrlsCntr = new StackPane();

		cntrlsCntr.getChildren().add(controls);
		cntrlsCntr.setAlignment(Pos.BASELINE_CENTER);
		add(cntrlsCntr, 2, 4, 7, 3);

		GridPane spaceRoot = new GridPane();

	}

	private void populate() {
		cont = 0;
		for (int c = C - 1; c > 0; c--) {
			this.add(buildSpace(cont, R - 1, c), c, R - 1);
		}

		for (int r = R - 1; r > 0; r--) {
			this.add(buildSpace(cont, r, 0), 0, r);
		}

		for (int c = 0; c < C - 1; c++) {
			this.add(buildSpace(cont, 0, c), c, 0);
		}
		for (int r = 0; r < R - 1; r++) {
			this.add(buildSpace(cont, r, C - 1), C - 1, r);
		}
	}

	private Pane buildSpace(int ind, int r, int c) {
		Tooltip toolTip = new Tooltip();
		toolTip.setText("TExt");
		GridPane spacePane = new GridPane();

		final ColumnConstraints colConst = new ColumnConstraints();
		final RowConstraints rowConst = new RowConstraints();

		colConst.setPercentWidth(33.33);
		rowConst.setPercentHeight(33.33);

		// tokenSpace.setStyle("-fx-background-color: black, white ;");

		spacePane.getColumnConstraints().addAll(colConst, colConst, colConst);
		spacePane.getRowConstraints().addAll(rowConst, rowConst, rowConst);
		spacePane.getStyleClass().add("board-cell");
		if (r == 0) {
			spacePane.setPadding(new Insets(0, 0, 35, 0));
			spacePane.getStyleClass().add("first-rw");
		} else if (r == R - 1) {
			spacePane.setPadding(new Insets(35, 0, 0, 0));
			spacePane.getStyleClass().add("last-rw");
		}
		if (c == 0) {
			spacePane.setPadding(new Insets(0, 35, 0, 0));
			spacePane.getStyleClass().add("first-col");
		} else if (c == C - 1) {
			spacePane.setPadding(new Insets(0, 0, 0, 35));
			spacePane.getStyleClass().add("last-col");
		}
		// spacePane.setOnMouseClicked(value);
		// spacePane.setGridLinesVisible(true);
		// spaces.add(spacePane);
		// System.out.println(cont);
		// spacePane.
		try {
			Space space = bConfig.getSpaces().get(cont);
			Space curr = bConfig.getSpaces().get(cont);
			GridPane infoBox = new GridPane();
			final ColumnConstraints cons = new ColumnConstraints();
			final RowConstraints rowst= new RowConstraints();
			
			rowst.setPercentHeight(50);
			
			cons.setPercentWidth(50);
			infoBox.getColumnConstraints().addAll(cons, cons);
			infoBox.getRowConstraints().addAll(rowst,rowst);

			HBox lbHolder = new HBox();
			HBox valHolder = new HBox();

			Label lblName = new Label("SPACE NAME");
			lbHolder.getChildren().add(lblName);
			Label lblVal = new Label(space.getName());

			infoBox.add(lbHolder, 0, 0);
			infoBox.add(valHolder, 1, 0);

			HBox lbHolder2 = new HBox();
			HBox valHolder2 = new HBox();
			lbHolder2.getChildren().add(new Label("Owner"));
			infoBox.add(lbHolder2,0,1);
			
			PropertySpace prop = space.getProperty();
			if (prop != null)
				if (prop.getDeed() != null) {

					if (prop.getDeed().getOwner() != null) {
						valHolder2.getChildren().add(new Label(prop.getDeed().getOwner().getName()));
						
					} else {
						valHolder.getChildren().add(new Label("This is a free space"));
					}
					infoBox.add(valHolder2,1,1);
				}

			valHolder.getChildren().add(lblVal);

//			if (curr.getProperty() != null) {
//				// Label lblName = new Label("SPACE NAME");
//				// lbHolder.getChildren().add(lblName);
//				// Label lblVal = new
//				// Label(bConfig.getSpaces().get(cont).getName());
//				// valHolder.getChildren().add(lblVal);
//			}

			// infoBox.getChildren().addAll(lbHolder,valHolder);
			Dialog spaceDialog;
			spaceDialog = new Dialog();
			spaceDialog.scene = new Scene(infoBox, 500, 500);
			spaceDialog.scene.getStylesheets().add(GameUIConstants.MASTER_CSS);
			spaceDialog.setScene(spaceDialog.scene);

			spacePane.setOnMouseClicked(new EventHandler<Event>() {

				@Override
				public void handle(Event event) {

					spaceDialog.show();

				}

			});
			UISpace uiSpace = new UISpace(spacePane);

			spaces.add(uiSpace);
			cont++;
			return spaces.get(cont - 1).getPane();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("null return ");
		return null;
	}

	private void layoutNodes() {

		final RowConstraints rowsEdge = new RowConstraints();
		rowsEdge.setPercentHeight(14);
		final RowConstraints rowsMid = new RowConstraints();
		rowsMid.setPercentHeight(8);

		final ColumnConstraints colEdge = new ColumnConstraints();
		colEdge.setPercentWidth(14);

		final ColumnConstraints colMid = new ColumnConstraints();
		colMid.setPercentWidth(8);

		this.getColumnConstraints().addAll(colEdge, colMid, colMid, colMid, colMid, colMid, colMid, colMid, colMid,
				colMid, colEdge);
		this.getRowConstraints().addAll(rowsEdge, rowsMid, rowsMid, rowsMid, rowsMid, rowsMid, rowsMid, rowsMid,
				rowsMid, rowsMid, rowsEdge);

		BackgroundImage boardimg = new BackgroundImage(new Image(GameUIConstants.MONOPOLY_BOARD),
				BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
				BackgroundSize.DEFAULT);
		// final StackPane imagePane = new StackPane();
		// this.add( imagePane, 0, 0, 11,11 );
		//
		// final DoubleBinding multipliedHeight =
		// this.heightProperty().multiply(1);
		// final DoubleBinding multipliedWidth = this.widthProperty().multiply(
		// 1 );
		//
		// imagePane.maxHeightProperty().bind( multipliedHeight );
		// imagePane.maxWidthProperty().bind( multipliedWidth );
		// imagePane.minHeightProperty().bind( multipliedHeight );
		// imagePane.minWidthProperty().bind( multipliedWidth );
		// imagePane.prefHeightProperty().bind( multipliedHeight );
		// imagePane.prefWidthProperty().bind( multipliedWidth );
		//
		// final ImageView imageView =
		// new ImageView(MonopolyConstants.MONOPOLY_BOARD);
		// imageView.setPreserveRatio( true );
		// imageView.fitWidthProperty().bind( imagePane.widthProperty());
		// imagePane.getChildren().add( imageView );

		this.setBackground(new Background(boardimg));
		// setGridLinesVisible(true);
	}

	public GameControl getControls() {
		return controls;
	}

	public void setToken(int playerId, UiToken tok) {
		tokens.put(playerId, tok);
		tok.setCurrentSpace(0);

		try {
			getSpaces().get(0).getPane().add(tok, tok.getPosition().c, tok.getPosition().r);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void move(int playerId, int value) {
		UiToken token = tokens.get(playerId);
		int curr = token.getCurrentSpace();
		int f = 0;

		if (curr + value <= BoardConfig.MAX_SPACES - 1)
			f = curr + value;
		else {
			f = (curr + value) - (BoardConfig.MAX_SPACES);
		}
		System.out.println("F :" + f);
		try {
			getSpaces().get(curr).getPane().getChildren().remove(token);
			getSpaces().get(f).getPane().getChildren().add(token);
			token.setCurrentSpace(f);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void moveTokenTo(int pId, int dest) {
		UiToken token = tokens.get(pId);
		if (token.getCurrentSpace() == dest)
			return;
		try {

			getSpaces().get(token.getCurrentSpace()).getPane().getChildren().remove(token);
			getSpaces().get(dest).getPane().getChildren().add(token);
			token.setCurrentSpace(dest);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public Map<Integer, UiToken> getTokens() {
		return tokens;
	}

	public UiToken getToken(int id) {
		return tokens.get(id);
	}

	public BoardConfig getBConfig() {
		return bConfig;
	}

	public Space getSpaceDetails(int ind) throws RemoteException {
		return bConfig.getSpaces().get(ind);
	}

	public void setBoardConfig(BoardConfig conf) {
		this.bConfig = conf;
	}

	public ArrayList<UISpace> getSpaces() throws RemoteException {
		return spaces;
	}

	public ArrayList<Card> getCommunityChestCards() throws RemoteException {
		return bConfig.getCommunityChestCards();
	}

	public ArrayList<Card> getChancesCards() throws RemoteException {
		return bConfig.getChanceCards();
	}

	public void makeDialog(Space sp) {

	}
}
