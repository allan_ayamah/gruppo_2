package client.ui.board;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class GameControl extends GridPane {

	private Button btnRoll, btnDrawCard, btnBuyProp, btnSellProp, btnEndTurn;

	private String[] controls = { "Roll", "Draw", "By prop", "Sell prop" };

	private Die d1, d2;
	private Label lblDice;

	public Label getLblDice() {
		return lblDice;
	}

	public GameControl(final EventHandler<Event> evHandler) {
		d1 = new Die();
		d2 = new Die();
		lblDice = new Label();
		lblDice.setTextFill(Color.WHITE);
		lblDice.setAlignment(Pos.CENTER);
		

		btnRoll = new Button("ROLL");
		btnRoll.addEventHandler(ActionEvent.ACTION, evHandler);
		btnDrawCard = new Button("DRAW CARD");
		btnDrawCard.addEventHandler(ActionEvent.ACTION, evHandler);
		btnBuyProp = new Button("BUY PROPERTY");
		btnBuyProp.addEventHandler(ActionEvent.ACTION, evHandler);
		btnSellProp = new Button("SELL PROPERTY");
		btnSellProp.addEventHandler(ActionEvent.ACTION, evHandler);
		btnEndTurn = new Button("END TURN");
		btnEndTurn.addEventHandler(ActionEvent.ACTION, evHandler);

		layoutNodes();
		setGridLinesVisible(false);

	}

	private void layoutNodes() {
		final ColumnConstraints colControls1 = new ColumnConstraints();
		colControls1.setPercentWidth(30);
		final ColumnConstraints colControls2 = new ColumnConstraints();
		colControls2.setPercentWidth(70);

		final RowConstraints rConstraint = new RowConstraints();
		rConstraint.setPercentHeight(75);
		final RowConstraints rConstraint2 = new RowConstraints();
		rConstraint2.setPercentHeight(25);

		getColumnConstraints().addAll(colControls1, colControls2);
		getRowConstraints().addAll(rConstraint, rConstraint2);
		// setHgap(10);
		// setVgap(10);

		VBox colCtrl1Cntr = new VBox();
		colCtrl1Cntr.setSpacing(15);
		colCtrl1Cntr.getChildren().addAll(btnRoll, btnEndTurn, btnDrawCard);

		HBox colCntrl2Cntr = new HBox();
		colCntrl2Cntr.setSpacing(15);
		colCntrl2Cntr.getChildren().addAll(btnBuyProp, btnSellProp);

		HBox diceCtnr = new HBox();
		diceCtnr.setSpacing(70);
		diceCtnr.setPadding(new Insets(10, 10, 0, 10));
		diceCtnr.getChildren().addAll(d1, d2);
		
		HBox cnDiceRes = new HBox();
		cnDiceRes.getChildren().add(lblDice);
		add(colCtrl1Cntr, 0, 0, 1, 2);
		add(diceCtnr, 1, 0, 1, 2);
		add(colCntrl2Cntr, 1, 1);
		add(cnDiceRes, 0,1);
	}

	public Button getBtnEndTurn() {
		return btnEndTurn;
	}

	public Button getBtnRoll() {
		return btnRoll;
	}

	public Button getBtnDrawCard() {
		return btnDrawCard;
	}

	public Button getBtnBuyProp() {
		return btnBuyProp;
	}

	public Button getBtnSellProp() {
		return btnSellProp;
	}

	public String[] getControls() {
		return controls;
	}

	public void stopRoll() {
		Platform.runLater(() -> {
			d1.stopDie();
			d2.stopDie();
		});

	}

	public void startsRoll() {
		Platform.runLater(() -> {
			d1.startDie();
			d2.startDie();

		});

	}
}
