package client.ui.board.space;
import java.io.Serializable;

public class PanePos implements Serializable{
	public int r = 0;
	public int c = 0;

	public PanePos(int row, int col) {
		r = row;
		c = col;
	}
	
}