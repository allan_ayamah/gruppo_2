package client.ui.board.space;

import javafx.scene.layout.GridPane;

public class UISpace{
	
	private GridPane pane;
	
	public UISpace( GridPane p) {
		pane = p;
	}

	public GridPane getPane() {
		return pane;
	}
}
