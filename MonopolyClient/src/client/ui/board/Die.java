package client.ui.board;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

public class Die extends Box{
    double size = 75;
    Timeline animation;
    public Die() {
         setWidth(size);
         setHeight(size);
         setDepth(size);
      
         setMaterial(new PhongMaterial(Color.RED));

        getTransforms().addAll(new Rotate(25, Rotate.Z_AXIS),new Rotate(75, Rotate.X_AXIS));
      

        animation = new Timeline();
        animation.getKeyFrames().addAll(
                new KeyFrame(Duration.ZERO,
                new KeyValue(rotationAxisProperty(), Rotate.X_AXIS),
                new KeyValue(rotateProperty(), 0d)),
                new KeyFrame(Duration.seconds(1),
                new KeyValue(rotationAxisProperty(), Rotate.X_AXIS),
                new KeyValue(rotateProperty(), 360d)));
        animation.setCycleCount(3);
        
	}
    
    public void stopDie(){
    	Platform.runLater(() ->{
    		animation.stop();	
    	});
    	
    }
    
    public void startDie(){
    	Platform.runLater(() ->{
    		animation.play();	
    	});
    }
    
    
}
