package client.ui;

import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Dialog extends Stage {
	
	public Stage parent;
	public  Scene scene;
	protected Dialog instace;
	
	public Dialog() {
		// TODO Auto-generated constructor stub
	}
	
	public Dialog(final Stage parent, boolean modal) {
		this.parent = parent;
		initModality(Modality.APPLICATION_MODAL);
		if(modal){
			setOnCloseRequest(e -> {
				close();
				parent.close();
			});
		}
		
		instace = this;
		
		
//		setWidth(400);
//		setHeight(250);
	}
	
	
	public void initDialog(){
		
	}
	


}
