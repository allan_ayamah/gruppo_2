package client.ui;

public class GameUIConstants {
	public static final String MASTER_CSS = "monopoly.css";
	public static final String WELCOME_IMAGE = "assets/welcome.png";
	public static final String MONOPOLY_BOARD= "assets/board.png";

}
