package client;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import client.ui.GameUIConstants;
import client.ui.board.GameBoard;
import client.ui.board.GameControl;
import client.ui.board.UiToken;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import shared.Message;
import shared.action.TurnAction;
import shared.bean.Dice;
import shared.bean.PlayerInfoBean;
import shared.bean.SessionBean;
import shared.board.BoardConfig;
import shared.exception.GameException;
import shared.notification.EventType;
import shared.notification.GameEvent;
import shared.player.AbstractSessionPlayer;
import shared.service.GameSession;

public class GameClient extends AbstractSessionPlayer {

	private static final long serialVersionUID = -6195003196794872129L;

	private BoardConfig bConfig;
	private String name;
	private int id;

	private int currentCash;
	private TitledPane cashPane;

	private PlayerStatus playerStatus = PlayerStatus.FREE;

	private GameSession session;
	private String sessionID = null;

	private Stage mainStage;
	private Scene scene;
	private BorderPane root;

	private GameBoard board;
	private GameControl controls;

	private TitledPane messageBox;
	private VBox playerList;
	private Text txtMessage;

	private Label lblCurrentCash;

	private VBox assetsBox;
	private Stage wStage;

	private ArrayList<Integer> deeds;

	private boolean outOfJailCard = false;

	public GameClient(String name) throws IOException {
		super();
		this.name = name;
	}

	public GameClient(String name, Stage stage) throws IOException {
		this(name);
		mainStage = stage;
	}

	// Funzione invocato dal server
	@Override
	public void initPlayground(SessionBean sessionDetails) throws RemoteException {
		session = sessionDetails.getGameSession();
		sessionID = sessionDetails.getSessionId();
		id = sessionDetails.getPlayerId();

		try {
			System.out.println("GameClient.initPlayground()");
			bConfig = sessionDetails.getBoardConfiguration();
			initGraphics(sessionDetails);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void initGraphics(SessionBean bean) throws IOException {
		Platform.runLater(() -> {
			wStage.close();
			System.out.println("GameClient.initGraphics()");

			try {
				root = (BorderPane) FXMLLoader.load(getClass().getResource("Playground.fxml"));
				controls = new GameControl(new ControllerEventHandler());
				board = new GameBoard(bean.getBoardConfiguration(), controls);
				System.out.println("initGraphics() 1");
				board.setPrefSize(808, 808);
				board.setMaxSize(808, 808);

				root.setCenter(board);

				scene = new Scene(root, 1200, 960);
				scene.getStylesheets().add(GameUIConstants.MASTER_CSS);
				mainStage.setScene(scene);

				mainStage.setTitle("MONOPOLY- GRUPPO 2 -> " + getName());

				txtMessage = new Text();
				txtMessage.setWrappingWidth(300);
				txtMessage.setText("MAY THE GAME BEGIN!!");
				messageBox = (TitledPane) root.lookup("#messageBox");
				messageBox.setContent(txtMessage);

				playerList = (VBox) root.lookup("#playerList");
				for (int i = 0; i < bean.getSetPlayersInfo().size(); i++) {

					TitledPane tPane = new TitledPane(bean.getSetPlayersInfo().get(i).getName(),
							new Text("Player info"));
					tPane.setMinHeight(50);
					Circle c = new Circle(10.2f);
					c.setFill(UiHelper.COLORS[bean.getSetPlayersInfo().get(i).getId()].brighter());
					tPane.setGraphic(c);
					playerList.getChildren().add(tPane);
					board.setToken(bean.getSetPlayersInfo().get(i).getId(),
							UiHelper.getTokenByCode(bean.getSetPlayersInfo().get(i).getId()));
				}

				cashPane = (TitledPane) root.lookup("#cashPane");
				lblCurrentCash = new Label();
				updateCash();
				cashPane.setContent(lblCurrentCash);

				assetsBox = (VBox) root.lookup("#assetBox");
				buildAssets();
				mainStage.setMaximized(true);
				mainStage.show();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		});
	}

	@Override
	public void notifyMessage(Message message) throws RemoteException {
		feedBackUpdater(message.getMessage());
	}

	private void feedBackUpdater(String message) {
		Platform.runLater(() -> {
			txtMessage.setText(message);
		});
	}

	@Override
	public void notifyGameUpdate(GameEvent event) throws RemoteException {
		Platform.runLater(() -> {

			if (EventType.PLAYER_JOINED.equals(event.getType())) {
				// PlayerInfoBean player = event.getSender();
				//
				// board.setToken(player.getId(), new UiToken(Color.RED));
			}

			if (EventType.GAME_START.equals(event.getType())) {

			}

			if (EventType.ROLL_DICE.equals(event.getType())) {
				rollD();
			}

			if (EventType.GOTO.equals(event.getType()) || EventType.DOUBLES.equals(event.getType())) {
				// controls.stopRoll();
				updateDiceResult(event.getDice());
				moveTo(event.getSender().getId(), event.getMoveVal());
				try {
					String add = bConfig.getSpaces().get(event.getMoveVal()).getName();
					if (event.getSender() != null && event.getSender().getId() == id)
						feedBackUpdater(event.getSenderMessage() + add);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			if (EventType.TAKE_TURN.equals(event.getType())) {
				// do activate BUTTONS
			}
			// if(event.getSender() != null && event.getSender().getId() == id)
			// feedBackUpdater(event.getSenderMessage());
			// if (event.getMessage() != null && !event.getMessage().equals(""))
			// {
			// feedBackUpdater(event.getMessage());
			// }

		});

	}

	private void updateDiceResult(Dice roll) {
		Platform.runLater(() -> {
			controls.getLblDice().setText(roll.getFirstRoll() + " + " + roll.getSecondRoll());
		});
	}

	private void rollD() {
		controls.startsRoll();
	}

	@Override
	public String getName() throws RemoteException {
		return name;
	}

	@Override
	public int getId() throws RemoteException {
		return id;
	}

	@Override
	public void move(int dest) throws RemoteException {

	}

	private int currentPlayer() {
		try {
			return getSession().getCurrentPlayerId();
		} catch (RemoteException e) {
			return -1;

		}
	}

	public void moveToken(int id, int val) {
		Platform.runLater(() -> {
			board.move(id, val);
		});
	}

	private void moveTo(int pId, int dest) {

		board.moveTokenTo(pId, dest);

	}

	public int getCurrentCash() {
		return currentCash;
	}

	public GameEvent prepareEvent(EventType type) {
		GameEvent ev = new GameEvent(type);
		try {
			ev.setSender(new PlayerInfoBean(getId(), getName()));
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ev;
	}

	public GameEvent prepareEventWithMessage(EventType type, String mess) {
		GameEvent ev = prepareEvent(type);
		ev.setMessage(mess);
		return ev;
	}

	public void getOutOfJail() throws RemoteException {
		playerStatus = PlayerStatus.FREE;

	}

	public GameSession getSession() {
		return session;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void addCash(int amount) {
		currentCash += amount;
		updateCash();
	}

	public void subCash(int amount) {
		currentCash -= amount;
		updateCash();
	}

	private void updateCash() {
		Platform.runLater(() -> {
			lblCurrentCash.setText("" + getCurrentCash());
		});
	}

	public void setId(int id) throws RemoteException {
		this.id = id;
	}

	public UiToken getToken() throws RemoteException {
		return board.getToken(getId());
	}

	public int getCurrentSpace() throws RemoteException {
		return getToken().getCurrentSpace();
	}

	public void setSession(GameSession se) {
		this.session = se;
	}

	private void buildAssets() throws RemoteException {
		for (int i = 0; i < UiHelper.COLORS.length; i++) {
			Label lb = new Label();
			lb.setText("You don't own any at the moment");
			lb.setMinHeight(50);
			TitledPane pane = new TitledPane("" + UiHelper.COLOR_NAMES[i], lb);
			Circle c = new Circle(10.6f);
			c.setFill(UiHelper.COLORS[i].darker());
			pane.setGraphic(c);
			pane.setPadding(new Insets(0, 0, 10, 0));

			int colorCount = 0;
			// for(int ind: getProperties()){
			// if(bConfig.getSpaces().get(ind).getGroup().getCode() == i){
			// colorCount++;
			// }
			// }
			// lb.setText(""+colorCount);

			assetsBox.getChildren().add(pane);
		}

	}

	public void setWaitin(Stage w) {
		wStage = w;
	}

	@Override
	public void setAvalableActions(ArrayList<TurnAction> actions) throws RemoteException {

		enableDisableButtons(actions);

	}

	private void enableDisableButtons(ArrayList<TurnAction> actions) {
//		Platform.runLater(() -> {
//			for (TurnAction action : actions) {
//				System.out.println(action);
//				if (action.equals(TurnAction.ROLL) || action.equals(TurnAction.ROLL_FOR_FREEDOM))
//					controls.getBtnRoll().setDisable(false);
//				else
//					controls.getBtnRoll().setDisable(true);
//
//				if (actions.equals(TurnAction.PROPERTY_ACTIONS)) {
//					controls.getBtnBuyProp().setDisable(false);
//					controls.getBtnSellProp().setDisable(false);
//				} else {
//					controls.getBtnSellProp().setDisable(true);
//					controls.getBtnBuyProp().setDisable(true);
//				}
//
//				if (action.equals(TurnAction.END_TURN))
//					controls.getBtnEndTurn().setDisable(false);
//				else
//					controls.getBtnEndTurn().setDisable(true);
//
//				if (action.equals(TurnAction.DRAWCARD))
//					controls.getBtnDrawCard().setDisable(false);
//				else
//					controls.setDisable(true);
//			}
//		});
	}

	class ControllerEventHandler implements EventHandler<Event> {

		@Override
		public void handle(Event e) {
			Object evSource = e.getSource();

			if (evSource.equals(controls.getBtnRoll())) {
				try {
					// move(2);
					session.rollDice(getId());
				} catch (RemoteException | GameException e1) {
					txtMessage.setText(e1.getMessage());
				}

			}

			if (evSource.equals(controls.getBtnDrawCard())) {
				try {
					session.drawCard(getId());
				} catch (RemoteException | GameException e1) {
					txtMessage.setText(e1.getMessage());
				}

			}

			if (evSource.equals(controls.getBtnBuyProp())) {
				try {
					session.buyProperty(getId());
				} catch (RemoteException | GameException e1) {
					txtMessage.setText(e1.getMessage());
				}
			}

			if (evSource.equals(controls.getBtnSellProp())) {
				try {
					session.sellProperty(getId());
				} catch (RemoteException | GameException e1) {
					txtMessage.setText(e1.getMessage());
				}
			}

			if (evSource.equals(controls.getBtnEndTurn())) {
				try {
					session.endTurn(getId());
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

		}

	}

	@Override
	public PlayerStatus getStatus() throws RemoteException {
		return playerStatus;
	}

	@Override
	public void setProperties(ArrayList<Integer> contracts) throws RemoteException {
		deeds = contracts;

		// System.out.println(contracts.toString());

	}

	@Override
	public void initCash(int amount) throws RemoteException {
		currentCash = amount;
	}

	@Override
	public void setStatus(PlayerStatus status) throws RemoteException {
		playerStatus = status;

	}

	@Override
	public void setOutOfJailCard(boolean set) throws RemoteException {
		outOfJailCard = set;

	}

	@Override
	public void setCurrentSpace(int space) throws RemoteException {
		getToken().setCurrentSpace(space);

	}

}
