package client;


import java.util.ArrayList;

import client.ui.DataInput;
import client.ui.Dialog;
import client.ui.GameUIConstants;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class DataInputDialog extends Dialog {
	
	private BorderPane root;
	private GridPane grid;
	private Scene scene;

	private ArrayList<DataInput> data = new ArrayList<DataInput>();

	private Button btnOk, btnCancel;

	private DataInputDialog instance;
	private GameCreationEventHandler eventHandler;

	public DataInputDialog(final Stage parent, String okTitle, String cancelTitle) {
		super(parent, false);
		root = new BorderPane();
		
		eventHandler = new GameCreationEventHandler();
		btnOk = new Button(okTitle);
		
		btnCancel = new Button(cancelTitle);
		btnCancel.getStyleClass().add("btn-primary");
		btnCancel.addEventHandler(ActionEvent.ACTION, eventHandler);
		
	
		
		addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, eventHandler);
		
		grid = new GridPane();
		grid.setPrefWidth(500);
		grid.setPadding(new Insets(15,10,30,15));
		
		root.setCenter(grid);
		
		scene = new Scene(root);
		scene.getStylesheets().add(GameUIConstants.MASTER_CSS);
		setScene(scene);
	
	}
	
	@Override
	public void initDialog() {
		

		final ColumnConstraints lblCol = new ColumnConstraints();
		lblCol.setPercentWidth(25);
		final ColumnConstraints inpCol = new ColumnConstraints();
		inpCol.setPercentWidth(75);
		
		grid.getColumnConstraints().addAll(lblCol, inpCol);
		
		grid.setHgap(10);
		grid.setVgap(15);
		
		for(DataInput d: getData()){
			grid.addColumn(0, d.getLblData());
			grid.addColumn(1, d.getTxtData());
		}
		
		HBox hButtons = new HBox();
		hButtons.setSpacing(10);
	
		hButtons.getChildren().addAll(btnOk, btnCancel);
		
		grid.add(hButtons,1,2,2,1);
		
	}
	
	
	
	public ArrayList<DataInput> getData() {
		return data;
	}

	public void addDataInput(DataInput d){
		getData().add(d);
	}

	class GameCreationEventHandler implements EventHandler<Event>{

		@Override
		public void handle(Event ev) {
			if(ev.getSource().equals(btnCancel) || ev.getSource().equals(instance)){
				close();
			}
			
		}
		
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}
	
	
	

}
