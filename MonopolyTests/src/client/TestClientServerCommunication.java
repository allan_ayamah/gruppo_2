package client;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.rmi.Naming;
import java.rmi.RemoteException;

import org.junit.Before;
import org.junit.Test;

import client.GameClientForTest;
import server.game.GameServiceImpl;
import shared.bean.SessionBean;
import shared.exception.GameException;
import shared.player.SessionPlayer;
import shared.service.GameService;
import shared.service.GameSession;

public class TestClientServerCommunication {
	
	GameService serverClass;
	String serviceName = "GameService";
	
	GameSession session;
	
	GameServiceImpl gameService;
	
	@Before
	public void setUp() throws Exception {
		gameService = new GameServiceImpl();
		startServer();
		establishServerConnection();
	}
	
	
	public void startServer() {
		try {

			Naming.rebind(serviceName, gameService);
			System.out.println("GameService pronto");
		} catch (Exception e) {
			fail("Couldn't start server" + e.getMessage());
			e.printStackTrace();
		}

	}

	
	public void establishServerConnection() {
		try{
			serverClass = (GameService) Naming.lookup(serviceName);
			//assertNotEquals(null, serverClass);
			System.out.println("Connection established!");
		} catch(Exception e){
			fail("Can't find server");
		}
	}
	
	//@Test
	public void createGameSession() throws IOException{
		//assertNotEquals(null, serverClass);
		try {
			SessionPlayer player = new GameClientForTest("Allan");
			
			SessionBean sessionBean = serverClass.createSession(player);
			
			//AbstractSessionPlayer abClient = (AbstractSessionPlayer) player;
			
			session = sessionBean.getGameSession();
			
			System.out.println("Your sessionId is = "+sessionBean.getSessionId());
		
		} catch (RemoteException e) {
			fail(e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void joinSession() throws IOException, GameException{
		//assertNotEquals(null, serverClass);
		createGameSession();
		String session_id = "session_0";
		try {
			GameClientForTest player = new GameClientForTest("Dimmo");
			
			serverClass.joinSession(session_id, player);
			System.out.println("Client is :"+player.getName());
			
			//session.startGame();
			
			System.out.println(session.getBoardConfiguration().getSpaces().size());
			
		} catch (RemoteException e) {
			fail(e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	
	

}
