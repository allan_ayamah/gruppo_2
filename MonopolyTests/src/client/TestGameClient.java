package client;

import static org.junit.Assert.*;

import java.rmi.Naming;

import org.junit.Before;
import org.junit.Test;

import com.sun.media.jfxmedia.events.PlayerTimeListener;

import server.game.GameServiceImpl;
import shared.bean.SessionBean;
import shared.service.GameService;

public class TestGameClient {
	GameService serverClass;
	GameClientForTest player;
	@Before
	public void setUp() throws Exception {
		GameServiceImpl gameService = new GameServiceImpl();
		try{
			Naming.rebind("GameService", gameService);
		}catch(Exception e){
			fail("Couldn't start server" + e.getMessage());
		}
		
		serverClass = (GameService) Naming.lookup("GameService");
		SessionBean sDetails = serverClass.createSession(player);
		
//		GameClientForTest p2 = new GameClientForTest("Player_2");	
	}
	
	@Test
	public void clientHasInitialValues(){
		assertNotNull(player.getCurrentCash());
		assertEquals(5,player.getPropertiesId().size());
	}

}
