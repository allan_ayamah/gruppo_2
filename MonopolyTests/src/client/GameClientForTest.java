package client;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.text.DefaultEditorKit.CutAction;

import client.ui.GameUIConstants;
import client.ui.board.GameBoard;
import client.ui.board.GameControl;
import client.ui.board.UiToken;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import shared.Message;
import shared.action.TurnAction;
import shared.bean.Dice;
import shared.bean.PlayerInfoBean;
import shared.bean.SessionBean;
import shared.board.BoardConfig;
import shared.exception.GameException;
import shared.notification.EventType;
import shared.notification.GameEvent;
import shared.player.AbstractSessionPlayer;
import shared.service.GameSession;

public class GameClientForTest extends AbstractSessionPlayer {

	private static final long serialVersionUID = -6195003196794872129L;

	private BoardConfig bConfig;
	private String name;
	private int id;

	private int currentCash;
	private TitledPane cashPane;

	private PlayerStatus playerStatus = PlayerStatus.FREE;

	private GameSession session;
	private String sessionID = null;

	private ArrayList<Integer> deeds;

	private boolean outOfJailCard = false;

	private int currentSpace;

	public GameClientForTest(String name) throws IOException {
		super();
		this.name = name;
	}

	public GameClientForTest(String name, Stage stage) throws IOException {
		this(name);
	}

	// Funzione invocato dal server
	@Override
	public void initPlayground(SessionBean sessionDetails) throws RemoteException {
		session = sessionDetails.getGameSession();
		sessionID = sessionDetails.getSessionId();
		id = sessionDetails.getPlayerId();
		System.out.println("GameClient.initPlayground()");
		bConfig = sessionDetails.getBoardConfiguration();
	}

	@Override
	public void notifyMessage(Message message) throws RemoteException {
		System.out.println(message.getMessage());
	}

	@Override
	public void notifyGameUpdate(GameEvent event) throws RemoteException {

		if (EventType.PLAYER_JOINED.equals(event.getType())) {
			// PlayerInfoBean player = event.getSender();
			//
			// board.setToken(player.getId(), new UiToken(Color.RED));
		}

		if (EventType.GAME_START.equals(event.getType())) {

		}

		if (EventType.ROLL_DICE.equals(event.getType())) {

		}

		if (EventType.GOTO.equals(event.getType()) || EventType.DOUBLES.equals(event.getType())) {

			moveTo(event.getSender().getId(), event.getMoveVal());

		}

		if (EventType.TAKE_TURN.equals(event.getType())) {
			// do activate BUTTONS
		}
		// if(event.getSender() != null && event.getSender().getId() == id)
		// feedBackUpdater(event.getSenderMessage());
		// if (event.getMessage() != null && !event.getMessage().equals(""))
		// {
		// feedBackUpdater(event.getMessage());
		// }

	}

	private void feedBackUpdater(String string) {
		System.out.println(string);

	}

	@Override
	public String getName() throws RemoteException {
		return name;
	}

	@Override
	public int getId() throws RemoteException {
		return id;
	}

	@Override
	public void move(int dest) throws RemoteException {

	}

	private int currentPlayer() {
		try {
			return getSession().getCurrentPlayerId();
		} catch (RemoteException e) {
			return -1;

		}
	}

	private void moveTo(int pId, int dest) {

		currentSpace = dest;
	}

	public int getCurrentCash() {
		return currentCash;
	}

	public void getOutOfJail() throws RemoteException {
		playerStatus = PlayerStatus.FREE;

	}

	public GameSession getSession() {
		return session;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void addCash(int amount) {
		currentCash += amount;
	}

	public void subCash(int amount) {
		currentCash -= amount;
	}

	public void setId(int id) throws RemoteException {
		this.id = id;
	}

	public int getCurrentSpace() throws RemoteException {
		return currentSpace;
	}

	public void setSession(GameSession se) {
		this.session = se;
	}

	private void buildAssets() throws RemoteException {

	}

	@Override
	public PlayerStatus getStatus() throws RemoteException {
		return playerStatus;
	}

	@Override
	public void setProperties(ArrayList<Integer> contracts) throws RemoteException {
		deeds = contracts;

	}
	
	
	public ArrayList<Integer> getPropertiesId(){
		return deeds;
	}

	@Override
	public void initCash(int amount) throws RemoteException {
		currentCash = amount;
	}

	@Override
	public void setStatus(PlayerStatus status) throws RemoteException {
		playerStatus = status;

	}

	@Override
	public void setOutOfJailCard(boolean set) throws RemoteException {
		outOfJailCard = set;

	}

	@Override
	public void setCurrentSpace(int space) throws RemoteException {
		currentSpace = space;

	}

	@Override
	public void setAvalableActions(ArrayList<TurnAction> actions) throws RemoteException {
		// TODO Auto-generated method stub

	}

}
