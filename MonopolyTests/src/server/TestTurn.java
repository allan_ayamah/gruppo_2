package server;

import static org.junit.Assert.*;

import java.io.IOException;
import java.rmi.RemoteException;

import org.junit.Before;
import org.junit.Test;

import client.GameClientForTest;
import server.action.Action;
import server.game.GameSessionImpl;
import shared.TurnState;
import shared.exception.SessionFullException;
import shared.player.SessionPlayer;

public class TestTurn {

	GameSessionImpl session;
	Turn turn ;
	@Before
	public void setUp() throws RemoteException{
		
		session = new GameSessionImpl();
		for (int i = 1; i <= 2; i++) {
			GameClientForTest player;
			try {
				player = new GameClientForTest("Player_" + i);
				session.addPlayer((SessionPlayer) player);
			} catch ( SessionFullException | IOException e) {
				fail("Player Joining error");

			}

		}
		
		turn = new Turn(session.getPlayer(0), session);
	}
	
	@Test
	public void testTurnState() throws RemoteException{
		System.out.println("---------- Turn ---------------");
		System.out.println("Before move State is :" +turn.state);
		assertEquals(TurnState.PRE_MOVE, turn.state);
		
		turn.move();
		
		System.out.println("After move state is :" + turn.state);
		//assertEquals(TurnState.POST_MOVE, turn.state);
		//assertEquals(TurnState.AGAIN, turn.state);
		System.out.println("---------- Finish Turn ---------------");
	}

}
