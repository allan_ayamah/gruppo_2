package server;

import static org.junit.Assert.fail;

import java.rmi.Naming;
import java.rmi.RemoteException;

import org.junit.Before;
import org.junit.Test;

import server.game.GameServiceImpl;

public class TestServer {

	String serviceName = "GameService";
	GameServiceImpl gameService;

	@Before
	public void setUp() throws Exception {
		gameService = new GameServiceImpl();
	}

	@Test
	public void startServer() {
		try {

			Naming.rebind(serviceName, gameService);

			System.out.println("GameService pronto");
		} catch (Exception e) {
			fail("Couldn't start server" + e.getMessage());
			e.printStackTrace();
		}

	}

	@Test
	public void stopServer() {
		try {
			Naming.unbind(serviceName);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			fail("Can't stop Server");
		}

	}

}
