package server.board;

import static org.junit.Assert.assertEquals;

import java.rmi.RemoteException;

import org.junit.Before;
import org.junit.Test;

import server.board.BoardConfigBuilder;
import server.board.BoardConfigImpl;
import server.board.DefaultBoardBuilder;
import shared.board.SpaceType;

public class BoardTest{

	private BoardConfigImpl board;
	private BoardConfigBuilder boardBuilder;

	@Before
	public void setUp() throws Exception {
		boardBuilder = new DefaultBoardBuilder();
		board = new BoardConfigImpl(boardBuilder);
	}

	@Test
	public void boardHas40Spacees() {
		assertEquals(40,board.getSpaces().size());
	}
	
	@Test
	public void testSpaceType() throws RemoteException{
		assertEquals(SpaceType.GO, board.getSpaces().get(0).getType());
	}
	
	@Test
	public void boardHas16ChanceCards(){
		assertEquals(16,board.getChanceCards().size());
	}
	
	@Test
	public void boardHas14CommunityChestCards(){
		assertEquals(14,board.getCommunityChestCards().size());
	}
	
}
