package server.action;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.rmi.RemoteException;

import org.junit.Before;
import org.junit.Test;

import client.GameClientForTest;
import server.game.GameSessionImpl;
import shared.exception.SessionFullException;
import shared.player.SessionPlayer;

public class ActionsTest {
	
	GameSessionImpl session;
	Action action;
	@Before
	public void setUp() throws RemoteException{
		
		session = new GameSessionImpl();
		for (int i = 1; i <= 2; i++) {
			GameClientForTest player;
			try {
				player = new GameClientForTest("Player_" + i);
				session.addPlayer((SessionPlayer) player);
			} catch ( SessionFullException | IOException e) {
				fail("Player Joining error");

			}

		}
		
		action = new Action();
	}
	
	@Test 
	public void testActionGetCash() throws RemoteException{
		int amount = 300;
		SessionPlayer p1 = session.getPlayer(0);
		System.out.println("----------------Get Cash------------------------");
		System.out.println(p1.getName()+", BEFORE GetCash :" + p1.getCurrentCash());
		
		action.setStrategy(new ActionGetCash(amount));
		action.applyAction(session, p1.getId());
		
		System.out.println(p1.getName()+", AFTER GetCash :" + p1.getCurrentCash());
		
		System.out.println("-----------------Finish Get Cash------------------------");
		
	}
	
	@Test
	public void testActionPay() throws RemoteException {
		int amount = 500;
		SessionPlayer p1 = session.getPlayer(0);
		SessionPlayer p2 = session.getPlayer(1);
		
		System.out.println("----------------Transaction-------------------------");
		System.out.println(p1.getName()+", BEFORE transaction :" + p1.getCurrentCash());
		System.out.println(p2.getName()+" BEFORE transaction :" + p2.getCurrentCash());
		
		action.setStrategy(new ActionPay(amount,p2.getId()));
		action.applyAction(session, p1.getId());
		
		System.out.println(p1.getName()+", AFTER transaction :" + p1.getCurrentCash());
		System.out.println(p2.getName()+" AFTER transaction :" + p2.getCurrentCash());
		
		System.out.println("-----------------Finish transaction------------------------");
	}
	
	
	@Test
	public void testActionDrawCard(){
		
//		SessionPlayer p1 = session.getPlayer(0);
//		
//		System.out.println("----------------Draw card-------------------------");
//		System.out.println(p1.getName()+", BEFORE Draw card :" + p1.get);
//		
//		action.setStrategy(new ActionPay(amount,p2.getId()));
//		action.applyAction(session, p1.getId());
//		
//		System.out.println(p1.getName()+", AFTER transaction :" + p1.getCurrentCash());
//		System.out.println(p2.getName()+" AFTER transaction :" + p2.getCurrentCash());
//		
//		System.out.println("-----------------Finish Draw card------------------------");
		
	}
	
	
	

}
