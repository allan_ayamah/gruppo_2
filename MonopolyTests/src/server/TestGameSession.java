package server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.rmi.RemoteException;

import org.junit.Before;
import org.junit.Test;

import client.GameClientForTest;
import server.game.GameSessionImpl;
import server.game.GameSessionImpl.SessionSate;
import shared.exception.GameException;
import shared.exception.SessionFullException;
import shared.player.AbstractSessionPlayer;
import shared.player.SessionPlayer;

public class TestGameSession {

	GameSessionImpl session;
	SessionPlayer masterPlayer;

	@Before
	public void setUp() throws Exception {
		session = new GameSessionImpl();
	}

	// @Test
	public void joinSession() throws IOException {
		GameClientForTest player = new GameClientForTest("Player_client");
		try {
			session.addPlayer(player);
			System.out.println("Added :" + player.getName() + " to " + session.getId());
		} catch (SessionFullException e) {
			fail(e.getMessage());
		} catch (RemoteException e) {
			fail("Remote error: adding player");
		}
	}

	// @Test
	public void addPlayers(int num) throws IOException {
		System.out.println("Test -> addPlayers()");
		for (int i = 1; i <= num; i++) {
			GameClientForTest player;
			try {
				player = new GameClientForTest("Player_" + i);
				session.addPlayer((SessionPlayer) player);
			} catch (RemoteException | SessionFullException e) {
				fail("Player Joining error");

			}

		}
	}

	// @Test
	public void testStartGame() throws IOException {
		System.out.println("Test -> " + "testStartGame()");
		addPlayers(3);
		try {
			session.startGame();
			assertNotNull(session.getCurrentplayer());
			assertEquals(SessionSate.IN_PROGRES, session.state);
			System.out.println("Starting player is -> " + session.getCurrentplayer().getName());
		} catch (GameException e) {
			fail(e.getMessage());
			e.printStackTrace();
		}
	}

	// @Test
	public void testSessionState() throws RemoteException {
		System.out.println("Test -> GameSession State");
		SessionSate state = SessionSate.IN_PROGRES;
		assertEquals(state, session.state);
	}

	//@Test
	public void rollDice() {
		try {
			addPlayers(3);
			session.startGame();
			int current = session.getCurrentplayer().getId();
			session.rollDice(current);

		} catch (IOException | GameException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
