package shared.board;

public enum PropertyColorGroup {
	YELLOW(0),
	PINK(1),
	BROWN(2),
	GREEN(3), 
	ORANGE(4),
	BLUE(5),
	PURPLE(6),
	RED(7);
	
	private int code;

	private PropertyColorGroup(int code) {
		this.code = code;
	}



	public int getCode() {
		return code;
	}
	
}
