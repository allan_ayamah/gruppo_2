package shared.board;

import java.rmi.Remote;
import java.rmi.RemoteException;

import shared.player.SessionPlayer;

public interface Space extends Remote {
	
	public String getName () throws RemoteException;
	public SpaceType getType() throws RemoteException;

	public PropertyColorGroup getGroup() throws RemoteException;

	public boolean isOwned() throws RemoteException;

	public void setOwner(SessionPlayer owner) throws RemoteException;

	public PropertySpace getProperty() throws RemoteException;

	public void setCanBuildOn(boolean canBuildOn) throws RemoteException;

	public boolean canBuildOn() throws RemoteException;
	public int getOwner() throws RemoteException;
}