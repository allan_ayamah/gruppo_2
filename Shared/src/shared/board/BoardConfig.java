package shared.board;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface BoardConfig extends Remote {
	public static final int GO = 0;
    public static final int JAIL = 10;
    public static final int FREE_PARKING = 20;
    public static final int GO_TO_JAIL = 30;
    
	public static final int MAX_SPACES = 40;
	public ArrayList<Space> getSpaces() throws RemoteException;
	public ArrayList<Card> getCommunityChestCards() throws RemoteException;
	public ArrayList<Card> getChanceCards()  throws RemoteException;
}
