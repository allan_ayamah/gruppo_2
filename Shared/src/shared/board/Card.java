package shared.board;

import shared.action.ActionStrategy;

public class Card {
	private String text;
	private ActionStrategy strategy;

	public ActionStrategy getStrategy() {
		return strategy;
	}
	public void setStrategy(ActionStrategy strategy) {
		this.strategy = strategy;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

}
