package shared.board;

import java.io.Serializable;

import shared.Deed;
import shared.player.SessionPlayer;

public class PropertySpace implements Serializable {
	protected Deed deed;
	protected PropertySpace property;
	protected SessionPlayer owner;
	protected boolean isOwned;
	
	private int houseNumber;
	private int hotelNumber;

	public PropertySpace(Deed d) {
		this.deed = d;
		setHotelNumber(0);
		setHouseNumber(0);
	}
	
	public int getHouseNumber(){
		return houseNumber;
	}
	public void setHouseNumber(int n){
		this.hotelNumber=n;
	}
	public int getHotelNumber(){
		return hotelNumber;
	}
	public void setHotelNumber(int n){
		this.hotelNumber=n;
	}
	
	public void setProperty(PropertySpace property) {
		this.property = property;
	}
	
	public PropertySpace getProperty() {
		return property;
	}

	public Deed getDeed() {
		return deed;
	}
	

}
