package shared;

import java.io.Serializable;

import javafx.scene.paint.Color;

public class Token implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8306187042518174834L;
	private String pathImage;
	
	private int currentSpace;

	public String getPathImage() {
		return pathImage;
	}

	public void setPathImage(String pathImage) {
		this.pathImage = pathImage;
	}

	public Color getColor() {
		return Color.AQUA;
	}
	
	
	public int getCurrentSpace() {
		return currentSpace;
	}
	
	public void setCurrentSpace(int val){
		currentSpace = val;
	}

}
