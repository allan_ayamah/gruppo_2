package shared;

import java.io.Serializable;

public class Message implements Serializable {
	
	protected int sender;
	protected String message;
	
	public Message(String mess){
		this.message = mess;
	}
	public Message(int player, String mess) {
		sender = player;
		message = mess;
	}

	public int getSender() {
		return sender;
	}

	public String getMessage() {
		return message;
	}

}
