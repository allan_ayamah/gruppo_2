package shared.action;

import java.rmi.Remote;
import java.rmi.RemoteException;

import shared.service.GameSession;

public interface ActionStrategy extends Remote{
	public void applyEffect(GameSession session, int playerId) throws RemoteException ;
}
