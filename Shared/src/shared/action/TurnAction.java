package shared.action;

public enum TurnAction {

	MOVE, PROPERTY_ACTIONS, END_TURN, ROLL_FOR_FREEDOM, BUY, SELL,PAY, USE_CARD, ROLL, DRAWCARD;

}
