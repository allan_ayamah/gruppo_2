package shared.player;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import shared.Message;
import shared.action.TurnAction;
import shared.bean.SessionBean;
import shared.notification.GameEvent;
import shared.player.AbstractSessionPlayer.PlayerStatus;

public interface SessionPlayer extends Remote {
	
	public String getName() throws RemoteException;
	public int getId() throws RemoteException;
	public void setId(int id) throws RemoteException;

	public int getCurrentCash() throws RemoteException;
	public void addCash(int amount) throws RemoteException;
	public void subCash(int amount) throws RemoteException;
	
	public void setStatus(PlayerStatus status) throws RemoteException;
	public void setOutOfJailCard(boolean set) throws RemoteException;
	public int getCurrentSpace() throws RemoteException;

	public void initPlayground(SessionBean sessionDetails) throws RemoteException;

	// Methods use by the server(GameSession) to communicate information to the
	
	public PlayerStatus getStatus() throws RemoteException;
	public void notifyMessage(Message message) throws RemoteException;

	public void notifyGameUpdate(GameEvent event) throws RemoteException;
	public void move(int value) throws RemoteException;



	public void setAvalableActions(ArrayList<TurnAction> actions) throws RemoteException;
	public void setProperties(ArrayList<Integer> contracts) throws RemoteException;
	public void initCash(int playerCash) throws RemoteException;
	public void setCurrentSpace(int space) throws RemoteException;
	
	
	
}