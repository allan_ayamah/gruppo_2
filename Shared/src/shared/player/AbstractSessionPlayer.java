package shared.player;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public abstract class AbstractSessionPlayer extends UnicastRemoteObject implements SessionPlayer{
	
	public enum PlayerStatus {
		FREE, IN_JAIL, BANKRUPT;
	}
	private static final long serialVersionUID = -3863065936271872876L;

	public AbstractSessionPlayer() throws RemoteException{
		super();
	}
	
	public AbstractSessionPlayer(String name) throws RemoteException{
		super();
	}

}	
