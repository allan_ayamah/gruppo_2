package shared.notification;

import java.rmi.RemoteException;

import shared.player.SessionPlayer;
import shared.service.GameSession;

public interface BroadcastStrategy{
	
	public void doBroadcast(GameSession session, SessionPlayer currPlayer) throws RemoteException;

}
