package shared.notification;

import java.io.Serializable;
import java.util.ArrayList;

import shared.action.TurnAction;
import shared.bean.Dice;
import shared.bean.PlayerInfoBean;

public class GameEvent implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4189491237465651279L;
	private EventType type;
	private String message;
	private String senderMessage;
	
	private ArrayList<TurnAction> availableActions;
	
	private PlayerInfoBean sender;
	
	private int moveVal;
	private Dice dice;
	
	public GameEvent() {
		// TODO Auto-generated constructor stub
	}
	
	public GameEvent(EventType event) {
		setType(event);
	}
	
	public EventType getType() {
		return type;
	}

	public void setType(EventType type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public PlayerInfoBean getSender() {
		return sender;
	}
	
	public void setSender(PlayerInfoBean bean){
		this.sender = bean;
	}

	public int getMoveVal() {
		return moveVal;
	}

	public void setMoveVal(int moveVal) {
		this.moveVal = moveVal;
	}

	public String getSenderMessage() {
		return senderMessage;
	}

	public void setSenderMessage(String senderMessage) {
		this.senderMessage = senderMessage;
	}

	public ArrayList<TurnAction> getAvailableActions() {
		return availableActions;
	}

	public void setAvailableActions(ArrayList<TurnAction> availableActions) {
		this.availableActions = availableActions;
	}

	public void setDiceVale(Dice diceRoll) {
		this.dice = diceRoll;
		
	}

	public Dice getDice() {
		return dice;
	}

	
	
	

}
