package shared.notification;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public abstract class AbBroadcastStrategy extends UnicastRemoteObject implements BroadcastStrategy {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9017882909594208177L;
	
	public AbBroadcastStrategy() throws RemoteException {
		super();
	}

}
