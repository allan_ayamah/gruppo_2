package shared.notification;

public enum EventType{
	PLAYER_JOINED("WELCOME","Player Joined"),
	GAME_START("","Game startin"),
	TAKE_TURN("IT'S YOUR TURN", ""),
	END_TURN("YOUR TURN HAS ENDED","Ending turn"),
	MOVEMENT("MOVED TO ","MOVED TO "),
	GOTO("YOUR CURRENT SPACE IS : ", ""),
	GAME_STATUS_CHANGE("session status",""), 
	ROLL_DICE("ROLL", "ROLL"), DOUBLES("DOUBLES, ROLL AGAIN","ROLLED DOUBLES"), CAUGHT_SPEEDING("YOU'RE SPEEDING", " WAS CAUGHT SPEEDING"), SELL("","");
	
	private GameEvent ev;
	private EventType(String nMess, String otherMes) {
		ev = new GameEvent(this);
		ev.setSenderMessage(nMess);
		this.ev.setMessage(otherMes);
	}
	
	public GameEvent getEvent(){
		return ev;
	}
}
