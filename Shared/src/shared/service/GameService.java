package shared.service;

import java.rmi.Remote;
import java.rmi.RemoteException;

import shared.bean.SessionBean;
import shared.player.SessionPlayer;

public interface GameService extends Remote{
	public String getSome(String name) throws RemoteException;
	public SessionBean createSession(SessionPlayer player) throws RemoteException;
	public SessionBean joinSession(String sessionId, SessionPlayer player) throws RemoteException;
	public void closeSession(GameSession session) throws RemoteException;

}
