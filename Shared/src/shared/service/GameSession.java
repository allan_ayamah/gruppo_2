package shared.service;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import shared.TurnState;
import shared.bean.PlayerInfoBean;
import shared.board.BoardConfig;
import shared.board.PropertyColorGroup;
import shared.exception.GameException;
import shared.notification.BroadcastStrategy;
import shared.player.SessionPlayer;

public interface GameSession extends Remote{
	public static final int MAX_PLAYERS = 8;
	public static final int MIN_PLAYERS = 2;
	
	public String getId() throws RemoteException;
	public void startGame() throws GameException, RemoteException;
	public BoardConfig getBoardConfiguration() throws RemoteException;
	public Collection<PropertyColorGroup> getAvailableTokens() throws RemoteException;
	public ArrayList<PlayerInfoBean> getPlayersInfo() throws RemoteException;
	
	public String getSessionId() throws RemoteException;
	
	public void rollDice(int playerId) throws RemoteException, GameException;
	public void buyProperty(int playerId) throws RemoteException, GameException;
	public void sellProperty(int playerId) throws RemoteException, GameException;
	
	public void drawCard(int playerId) throws RemoteException, GameException;
	
	public void broadcast(BroadcastStrategy strategy) throws RemoteException;
	public int getCurrentPlayerId() throws RemoteException;
	
	public void endTurn(int id) throws RemoteException;
	public TurnState getTurnState() throws RemoteException;
	public SessionPlayer getPlayer(int playerId) throws RemoteException;
}
