package shared.bean;

import java.io.Serializable;

public class Dice implements Serializable {
	
	private int firstRoll;
	private int secondRoll;
	
	public Dice(int f, int s) {
		firstRoll = f;
		secondRoll = s;
	}

	public int getFirstRoll() {
		return firstRoll;
	}

	public int getSecondRoll() {
		return secondRoll;
	}
	
	public int getValue(){
		return getFirstRoll() + getSecondRoll();
	}
	
	public boolean isDoubles(){
		return getFirstRoll() == getSecondRoll();
	}

}
