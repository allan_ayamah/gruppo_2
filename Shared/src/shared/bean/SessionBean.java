package shared.bean;

import java.io.Serializable;
import java.util.ArrayList;

import shared.board.BoardConfig;
import shared.service.GameSession;

public class SessionBean implements Serializable {

	private static final long serialVersionUID = 1939915527417404055L;

	protected String sessionId;
	protected GameSession gameSession;
	protected BoardConfig board;
	private int playerId;

	private ArrayList<PlayerInfoBean> setPlayersInfo;

	public SessionBean(String id, int playerId, GameSession gSession, BoardConfig board) {
		sessionId = id;
		gameSession = gSession;
		this.playerId = playerId;
		this.board = board;
	}

	public String getSessionId() {
		return sessionId;
	}

	public GameSession getGameSession() {
		return gameSession;
	}
	
	public int getPlayerId(){
		return playerId;
	}
	
	public BoardConfig getBoardConfiguration() {
		return board;
	}

	public void setPlayersInfo(ArrayList<PlayerInfoBean> playersInfo) {
		this.setPlayersInfo = playersInfo;
		
	}

	public ArrayList<PlayerInfoBean> getSetPlayersInfo() {
		return setPlayersInfo;
	}
	
	

}
