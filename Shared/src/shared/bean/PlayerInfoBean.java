package shared.bean;

import java.io.Serializable;

public class PlayerInfoBean implements Serializable {

	private static final long serialVersionUID = 5149880890136509298L;
	private String name;
	private int id;

	public PlayerInfoBean(int id,String name) {
		this.name = name;
		this.id = id;
	}

	
	public String getName() {
		return name;
	}

	public void setid(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

}
