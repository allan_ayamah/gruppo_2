
package shared;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;

import shared.player.SessionPlayer;

public class Deed implements Serializable {
	private int purchasePrice;
	private int mortgageValue;
	
	private int houseBuildingCost;
	private int hotelBuildingCost;
	private ArrayList<Integer> rentPrices;
	private SessionPlayer owner;
	
	public static final int MAX_RENT = 5;
	
	public Deed() {
		rentPrices = new ArrayList<Integer>(MAX_RENT);
		owner = null;
	}
	
	public SessionPlayer getOwner() {
		return owner;
	}

	public void setOwner(SessionPlayer owner) {
		this.owner = owner;
	}
	
	public int getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(int purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public int getMortgageValue() {
		return mortgageValue;
	}

	public void setMortgageValue(int mortgageValue) {
		this.mortgageValue = mortgageValue;
	}

	public int getHouseBuildingCost() {
		return houseBuildingCost;
	}

	public void setHouseBuildingCost(int houseBuildingCost) {
		this.houseBuildingCost = houseBuildingCost;
	}

	public int getHotelBuildingCost() {
		return hotelBuildingCost;
	}

	public void setHotelBuildingCost(int hotelBuildingCost) {
		this.hotelBuildingCost = hotelBuildingCost;
	}

	public void setRentPrice(Integer price){
		rentPrices.add(price);
	}
	

	public Integer getRentPrice(int key){
		if(key > MAX_RENT)
			return -1; // cant get price
		
		return rentPrices.get(key);
	}
	
	public int ownerId() throws RemoteException{
		return owner.getId();
	}
}
