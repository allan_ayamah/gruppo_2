<!-- data una query (memorizzata nell'URL con nome "query") restituisce il risultato sotto forma di tabella html-->
<%@page import="com.sun.xml.internal.ws.api.addressing.WSEndpointReference.Metadata"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*" %>
<%
	if((request.getParameter("query") != null) && (!request.getParameter("query").equals(""))){
		String query = request.getParameter("query");
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection c = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/progetto","root","root");
			Statement s = c.createStatement();
			ResultSet res = s.executeQuery(query);
			ResultSetMetaData meta =  res.getMetaData(); //valori medadata
			int numCol= meta.getColumnCount();

			out.println("<table></thead><tr>");
			for(int i = 1; i < numCol; i++){
				out.println("<th>"+meta.getColumnName(i)+"</th>");
			}
			out.println("</tr></thead><tbody>");
					
			while(res.next()){
				out.println("<tr>");
				for(int i = 1; i < numCol; i++){
					out.println("<td>"+res.getString(meta.getColumnName(i))+"</td>");
				}
				out.println("</tr>");
			}
			out.println("<tbody></table>");	
			
		}catch (ClassNotFoundException e) {
			out.print("error class");
			e.printStackTrace();
		} catch (SQLException e) {
			out.print("error sql");
			e.printStackTrace();
		}
	}
%>	