// funzioni generali 
function refreshPage(){
	//ogni tot secondi scarica le posizioni delle pedine e setta i div, le costruzioni e setta i div, tutta la grafica del gioco
	
	infoGiocatore(idGioc, idPart);
}


function load(){
	
	var n=8;  //query per sapere il n. di giocatori
	var i, k;
	
	//setto la pedina scelta dal gioc in tutte le posizioni
	for (i=0; i < 40; i++){
		for (k=0; k < n; k++){
			document.getElementById("l"+i+"_"+k).style.backgroundImage = 'url(http://localhost:8080/MonopolyWeb/image/'+k+'.png)';
		}
		for (k=1; k < 5; k++){
			document.getElementById("l"+i+"_c"+k).style.backgroundImage = 'url(http://localhost:8080/MonopolyWeb/image/casa.png)';
		}
		
		if (i==10){
			for (k=0; k < n; k++){
				document.getElementById("lp"+"_"+k).style.backgroundImage = 'url(http://localhost:8080/MonopolyWeb/image/'+k+'.png)';
			}
		}
	}
	
	
	//setto il background color di tutti i div e hidden tranne il primo
	for (i=0; i < 40; i++){
		document.getElementById("l"+i+"_0").style.backgroundColor = "red";
		document.getElementById("l"+i+"_1").style.backgroundColor = "yellow";
		document.getElementById("l"+i+"_2").style.backgroundColor = "#af80b7";
		document.getElementById("l"+i+"_3").style.backgroundColor = "pink";
		document.getElementById("l"+i+"_4").style.backgroundColor = "#fdfbfc";
		document.getElementById("l"+i+"_5").style.backgroundColor = "#ff9800";
		document.getElementById("l"+i+"_6").style.backgroundColor = "#07d4f5";
		document.getElementById("l"+i+"_7").style.backgroundColor = "#0fe43d";
		
		if (i == 10){
			document.getElementById("lp_0").style.backgroundColor = "red";
			document.getElementById("lp_1").style.backgroundColor = "yellow";
			document.getElementById("lp_2").style.backgroundColor = "#af80b7";
			document.getElementById("lp_3").style.backgroundColor = "pink";
			document.getElementById("lp_4").style.backgroundColor = "#fdfbfc";
			document.getElementById("lp_5").style.backgroundColor = "#ff9800";
			document.getElementById("lp_6").style.backgroundColor = "#07d4f5";
			document.getElementById("lp_7").style.backgroundColor = "#0fe43d";
			
		}
		
		/*if (i > 0){
			for (k=0; k < n; k++){
				document.getElementById("l"+i+"_"+k).style.visibility = "hidden";
				if (i == 10){
					document.getElementById("lp_"+k).style.visibility = "hidden";
				}
			}
		}  */
	}
	
	
	
	setInterval(refreshPage, 10);
	
	
	
}

function callAjax(url, stampaRisultato ){
	
	var ajax;
	if (window.XMLHttpRequest) {							// codice per browser moderni
		ajax = new XMLHttpRequest();
	    } else {											// codice per browser vecchi
	    	ajax = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	
	ajax.onreadystatechange = function(){
		if(ajax.readyState == 4 && ajax.status == 200){
			stampaRisultato(ajax);
		}
	};
	ajax.open("GET",url, true);
	ajax.send();
}

//////////////////////////////////////////////////////////////////////////////////////////////


//funzioni di risposta agli eventi del campo di gioco 


//////////////////////////////////////////////////////////////////////////////////////////////

// funzione che restituisce le informazioni del lotto cliccato nel div "infolotto"
// idQuery = 1
function getInfoLotto(idLotto,idPart){   
	
	var idPart; //prelevare l'id della partita
	
	var url="http://localhost:8080/MonopolyWeb/query?idLotto="+idLotto+"&idQuery=1&idPart="+idPart;
	
	callAjax(url, stampaLotto);
}

// funzione che mostra le informazioni del giocatore nel div "info giocatore"
//idQuery=19
function infoGiocatore(idGioc, idPart){  
	
	var url="http://localhost:8080/MonopolyWeb/query?idGioc="+idGioc+"&idPart="+idPart+"&idQuery=19";
	
	callAjax(url, stampaInfoGiocatore);
			
}

// onClick del button "ld"  
// idQuery=2
function lanciaDadi(){   
	
	var idGioc;
	var idPart;   //sessione???
	
	//funzione dadi 3d
	var val = Math.floor((Math.random()*11)+2);
	//stampare il valore in un div
	
	var url="http://localhost:8080/MonopolyWeb/query?idGioc="+idGioc+"&idPart="+idPart+"&idQuery=2";
	callAjax(url, setDadi);
}


// onclick del button "cc" 
function costruisciCasa(idGiocatore,IdPartita){
	
}		
	
	
/////////////////////////////////////////////////////////////////////////////////////////////


// funzioni di stampa del risultato sul campo da gioco nei vari div 


/////////////////////////////////////////////////////////////////////////////////////////////


// stampa le informazioni del lotto in una tabella nel div "infolotto"
function stampaLotto(ajax){
	
	var ajaxResult = ajax.responseXML;
	var intestazione = ajaxResult.documentElement.getElementsByTagName("colIntestazione");
	var riga = ajaxResult.documentElement.getElementsByTagName("col");
	
	
	var str = "<table>";	
	for(var i=0; i < intestazione.length; i++){
		if(riga[i].childNodes[0].nodeValue != 0){
			str += "<tr>";
			str += "<th>"+intestazione[i].childNodes[0].nodeValue+"</th>";
			str += "<td>"+riga[i].childNodes[0].nodeValue+"</td>";
			str += "</tr>";
		}
	}
	str += "</table>";
	
	document.getElementById("infoLotto").innerHTML = str;
	
}






	
function setDadi(ajax){   //3
	
	var ajaxResult = ajax.responseXML;
	var riga = ajaxResult.getElementsByTagName("col");
	var pos = riga[0].childNodes[0].nodeValue;
	var idGioc = riga[0].childNodes[1].nodeValue;
	var idPart = riga[0].childNodes[2].nodeValue;
	
	if ((pos+val) > 39){
		pos = (pos+val)-40;
	}
	else{
		pos += val;
	}
	
	var url = "http://localhost:8080/MonopolyWeb/query?idGioc="+idGioc+"&idPart="+idPart+"&idQuery=3&pos="+pos;
	ajax.open("GET",url, true);
	ajax.send();	
}	
	
	
	
	



	
	