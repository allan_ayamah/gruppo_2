//evidenzia l'ultima icona cliccata e memoriza la classe dell'immagine all'interno del campo "pedinaIco"
//obj -> rappresenta l'oggetto icona selezionato
//str -> indica il nome della glyphicon
//idPedina -> salvataggio del valore della pedina 
function selezionaIcona(obj,str,idForm){
	for (var i = 0; i < 8; i++) {
		document.getElementById("ico"+i).style.color = "#333";
	}
	
	obj.style.color = "#FFD700";
	document.getElementById(idForm).elements["pedinaIco"].value = str;
	alert(document.getElementById(idForm).elements["pedinaIco"].value);
}

//evidenzia l'ultima riga della tabella cliccata e memoriza il'idPartita all'interno del campo "tabellaPartite"
//obj -> rappresenta l'oggetto icona selezionato
//str -> indica il nome della glyphicon
//idPedina -> salvataggio del valore della pedina 
var objTabellaPrec = null;
function selezionaPartita(obj,idPartita){
	if(objTabellaPrec != null){
		objTabellaPrec.style.backgroundColor = "white";	
	}
	obj.style.backgroundColor = "rgba(255, 215, 0,0.5)";
	objTabellaPrec = obj;
	document.getElementById("idPartita").value = idPartita;
}

//funzione ajax che chiama una servlet jsp "tabLotti" che restituisce la tabella delle 
function caricaTabPartite(idDiv){
	var ajax;
	  if (window.XMLHttpRequest) {							// codice per browser moderni
	    ajax = new XMLHttpRequest();
	    } else {											// codice per browser vecchi
	    ajax = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	
	ajax.onreadystatechange = function(){
		if(ajax.readyState == 4 && ajax.status == 200){
			var ajaxResult = ajax.responseXML;
			var intestazione = ajaxResult.documentElement.getElementsByTagName("colIntestazione");
			var riga = ajaxResult.documentElement.getElementsByTagName("riga");

			/*creazione tabella*/
			var str = "<table class='table table-hover text-center'>";	
			str +="<thead>";
			for(var i=0; i < intestazione.length; i++){
				if(intestazione[i].childNodes[0].nodeValue == "nomeGiocatore"){
					str += "<th>nomePropietario</th>";
				}else{
					str += "<th>"+intestazione[i].childNodes[0].nodeValue+"</th>";
				}
			}
			str +="</thead></tbody>";
			
			for(var k=0; k < riga.length; k++){
				str += "<tr onclick='selezionaPartita(this,"+riga[k].getElementsByTagName("col")[0].childNodes[0].nodeValue+")'>";
				for(var i=0; i < intestazione.length; i++){
						str += "<td>"+riga[k].getElementsByTagName("col")[i].childNodes[0].nodeValue+"</td>";
				}
				str += "</tr>";	
				str += "</tbody></table>";		
				document.getElementById(idDiv).innerHTML = str;
			
			}
		}
	}
	
	ajax.open("get","http://localhost:8080/MonopolyWeb/query?idQuery=4",true); 
	ajax.send();
	
}
