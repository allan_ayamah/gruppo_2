<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>


<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="css/CG.css" />
<script type="text/javascript" src="js/campoGioco.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="image/favicon.ico" />

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

<title>Insert title here</title>
</head>

<%
	String idGioc= null;
	String idPartita= null;
   	if ((session.getAttribute("idGiocatore") == null) || (session.getAttribute("idPartita") == null)) {
		response.sendRedirect("http://localhost:8080/MonopolyWeb/index.jsp");
	} else {  
		idGioc = (String) session.getAttribute("idGiocatore");
		idPartita = (String) session.getAttribute("idPartita");
	} 

%>	


<body onload="load(<%=idGioc%>,<%=idPartita %>)">







	<div class="campo">

		<div id="l21" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l21_7"></div>
			<div id="l21_6"></div>
			<div id="l21_5"></div>
			<div id="l21_4"></div>
			<div id="l21_3"></div>
			<div id="l21_2"></div>
			<div id="l21_1"></div>
			<div id="l21_0"></div>

			<div id="l21_c1"></div>
			<div id="l21_c2"></div>
			<div id="l21_c3"></div>
			<div id="l21_c4"></div>
		</div>

		<div id="l22" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l22_7"></div>
			<div id="l22_6"></div>
			<div id="l22_5"></div>
			<div id="l22_4"></div>
			<div id="l22_3"></div>
			<div id="l22_2"></div>
			<div id="l22_1"></div>
			<div id="l22_0"></div>
		</div>

		<div id="l23" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l23_7"></div>
			<div id="l23_6"></div>
			<div id="l23_5"></div>
			<div id="l23_4"></div>
			<div id="l23_3"></div>
			<div id="l23_2"></div>
			<div id="l23_1"></div>
			<div id="l23_0"></div>

			<div id="l23_c1"></div>
			<div id="l23_c2"></div>
			<div id="l23_c3"></div>
			<div id="l23_c4"></div>
		</div>

		<div id="l24" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l24_7"></div>
			<div id="l24_6"></div>
			<div id="l24_5"></div>
			<div id="l24_4"></div>
			<div id="l24_3"></div>
			<div id="l24_2"></div>
			<div id="l24_1"></div>
			<div id="l24_0"></div>

			<div id="l24_c1"></div>
			<div id="l24_c2"></div>
			<div id="l24_c3"></div>
			<div id="l24_c4"></div>
		</div>

		<div id="l25" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l25_7"></div>
			<div id="l25_6"></div>
			<div id="l25_5"></div>
			<div id="l25_4"></div>
			<div id="l25_3"></div>
			<div id="l25_2"></div>
			<div id="l25_1"></div>
			<div id="l25_0"></div>
		</div>

		<div id="l26" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l26_7"></div>
			<div id="l26_6"></div>
			<div id="l26_5"></div>
			<div id="l26_4"></div>
			<div id="l26_3"></div>
			<div id="l26_2"></div>
			<div id="l26_1"></div>
			<div id="l26_0"></div>

			<div id="l26_c1"></div>
			<div id="l26_c2"></div>
			<div id="l26_c3"></div>
			<div id="l26_c4"></div>
		</div>

		<div id="l27" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l27_7"></div>
			<div id="l27_6"></div>
			<div id="l27_5"></div>
			<div id="l27_4"></div>
			<div id="l27_3"></div>
			<div id="l27_2"></div>
			<div id="l27_1"></div>
			<div id="l27_0"></div>

			<div id="l27_c1"></div>
			<div id="l27_c2"></div>
			<div id="l27_c3"></div>
			<div id="l27_c4"></div>
		</div>

		<div id="l28" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l28_7"></div>
			<div id="l28_6"></div>
			<div id="l28_5"></div>
			<div id="l28_4"></div>
			<div id="l28_3"></div>
			<div id="l28_2"></div>
			<div id="l28_1"></div>
			<div id="l28_0"></div>
		</div>

		<div id="l29" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l29_7"></div>
			<div id="l29_6"></div>
			<div id="l29_5"></div>
			<div id="l29_4"></div>
			<div id="l29_3"></div>
			<div id="l29_2"></div>
			<div id="l29_1"></div>
			<div id="l29_0"></div>

			<div id="l29_c1"></div>
			<div id="l29_c2"></div>
			<div id="l29_c3"></div>
			<div id="l29_c4"></div>
		</div>

		<div id="l30" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l30_0"></div>
			<div id="l30_1"></div>
			<div id="l30_2"></div>
			<div id="l30_3"></div>
			<div id="l30_4"></div>
			<div id="l30_5"></div>
			<div id="l30_6"></div>
			<div id="l30_7"></div>
		</div>

		<div id="l20" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l20_0"></div>
			<div id="l20_1"></div>
			<div id="l20_2"></div>
			<div id="l20_3"></div>
			<div id="l20_4"></div>
			<div id="l20_5"></div>
			<div id="l20_6"></div>
			<div id="l20_7"></div>
		</div>

		<div id="l10" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l10_4"></div>
			<div id="l10_5"></div>
			<div id="l10_6"></div>
			<div id="l10_7"></div>
			<div id="l10_3"></div>
			<div id="l10_2"></div>
			<div id="l10_1"></div>
			<div id="l10_0"></div>
			<div id="lp_0"></div>
			<div id="lp_4"></div>
			<div id="lp_1"></div>
			<div id="lp_2"></div>
			<div id="lp_3"></div>
			<div id="lp_5"></div>
			<div id="lp_6"></div>
			<div id="lp_7"></div>
		</div>

		<div id="l0" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l0_4"></div>
			<div id="l0_5"></div>
			<div id="l0_6"></div>
			<div id="l0_7"></div>
			<div id="l0_0"></div>
			<div id="l0_1"></div>
			<div id="l0_2"></div>
			<div id="l0_3"></div>
		</div>

		<div id="l9" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l9_0"></div>
			<div id="l9_4"></div>
			<div id="l9_1"></div>
			<div id="l9_2"></div>
			<div id="l9_3"></div>
			<div id="l9_5"></div>
			<div id="l9_6"></div>
			<div id="l9_7"></div>

			<div id="l9_c1"></div>
			<div id="l9_c2"></div>
			<div id="l9_c3"></div>
			<div id="l9_c4"></div>
		</div>

		<div id="l8" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l8_0"></div>
			<div id="l8_1"></div>
			<div id="l8_2"></div>
			<div id="l8_3"></div>
			<div id="l8_4"></div>
			<div id="l8_5"></div>
			<div id="l8_6"></div>
			<div id="l8_7"></div>

			<div id="l8_c1"></div>
			<div id="l8_c2"></div>
			<div id="l8_c3"></div>
			<div id="l8_c4"></div>
		</div>

		<div id="l6" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l6_0"></div>
			<div id="l6_1"></div>
			<div id="l6_2"></div>
			<div id="l6_3"></div>
			<div id="l6_4"></div>
			<div id="l6_5"></div>
			<div id="l6_6"></div>
			<div id="l6_7"></div>

			<div id="l6_c1"></div>
			<div id="l6_c2"></div>
			<div id="l6_c3"></div>
			<div id="l6_c4"></div>
		</div>

		<div id="l3" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l3_0"></div>
			<div id="l3_1"></div>
			<div id="l3_2"></div>
			<div id="l3_3"></div>
			<div id="l3_4"></div>
			<div id="l3_5"></div>
			<div id="l3_6"></div>
			<div id="l3_7"></div>

			<div id="l3_c1"></div>
			<div id="l3_c2"></div>
			<div id="l3_c3"></div>
			<div id="l3_c4"></div>
		</div>

		<div id="l1" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l1_0"></div>
			<div id="l1_1"></div>
			<div id="l1_2"></div>
			<div id="l1_3"></div>
			<div id="l1_4"></div>
			<div id="l1_5"></div>
			<div id="l1_6"></div>
			<div id="l1_7"></div>

			<div id="l1_c1"></div>
			<div id="l1_c2"></div>
			<div id="l1_c3"></div>
			<div id="l1_c4"></div>
		</div>

		<div id="l7" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l7_4"></div>
			<div id="l7_5"></div>
			<div id="l7_6"></div>
			<div id="l7_7"></div>
			<div id="l7_0"></div>
			<div id="l7_1"></div>
			<div id="l7_2"></div>
			<div id="l7_3"></div>
		</div>

		<div id="l5" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l5_4"></div>
			<div id="l5_5"></div>
			<div id="l5_6"></div>
			<div id="l5_7"></div>
			<div id="l5_0"></div>
			<div id="l5_1"></div>
			<div id="l5_2"></div>
			<div id="l5_3"></div>
		</div>

		<div id="l4" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l4_0"></div>
			<div id="l4_1"></div>
			<div id="l4_2"></div>
			<div id="l4_3"></div>
			<div id="l4_4"></div>
			<div id="l4_5"></div>
			<div id="l4_6"></div>
			<div id="l4_7"></div>
		</div>

		<div id="l2" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l2_0"></div>
			<div id="l2_1"></div>
			<div id="l2_2"></div>
			<div id="l2_3"></div>
			<div id="l2_4"></div>
			<div id="l2_5"></div>
			<div id="l2_6"></div>
			<div id="l2_7"></div>
		</div>

		<div id="l11" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l11_0"></div>
			<div id="l11_4"></div>
			<div id="l11_1"></div>
			<div id="l11_2"></div>
			<div id="l11_3"></div>
			<div id="l11_5"></div>
			<div id="l11_6"></div>
			<div id="l11_7"></div>

			<div id="l11_c1"></div>
			<div id="l11_c2"></div>
			<div id="l11_c3"></div>
			<div id="l11_c4"></div>
		</div>

		<div id="l13" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l13_4"></div>
			<div id="l13_5"></div>
			<div id="l13_6"></div>
			<div id="l13_7"></div>
			<div id="l13_0"></div>
			<div id="l13_1"></div>
			<div id="l13_2"></div>
			<div id="l13_3"></div>

			<div id="l13_c1"></div>
			<div id="l13_c2"></div>
			<div id="l13_c3"></div>
			<div id="l13_c4"></div>
		</div>

		<div id="l14" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l14_4"></div>
			<div id="l14_5"></div>
			<div id="l14_6"></div>
			<div id="l14_7"></div>
			<div id="l14_0"></div>
			<div id="l14_1"></div>
			<div id="l14_2"></div>
			<div id="l14_3"></div>

			<div id="l14_c1"></div>
			<div id="l14_c2"></div>
			<div id="l14_c3"></div>
			<div id="l14_c4"></div>
		</div>

		<div id="l16" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l16_4"></div>
			<div id="l16_5"></div>
			<div id="l16_6"></div>
			<div id="l16_7"></div>
			<div id="l16_0"></div>
			<div id="l16_1"></div>
			<div id="l16_2"></div>
			<div id="l16_3"></div>

			<div id="l16_c1"></div>
			<div id="l16_c2"></div>
			<div id="l16_c3"></div>
			<div id="l16_c4"></div>
		</div>

		<div id="l18" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l18_4"></div>
			<div id="l18_5"></div>
			<div id="l18_6"></div>
			<div id="l18_7"></div>
			<div id="l18_0"></div>
			<div id="l18_1"></div>
			<div id="l18_2"></div>
			<div id="l18_3"></div>

			<div id="l18_c1"></div>
			<div id="l18_c2"></div>
			<div id="l18_c3"></div>
			<div id="l18_c4"></div>
		</div>

		<div id="l19" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l19_4"></div>
			<div id="l19_5"></div>
			<div id="l19_6"></div>
			<div id="l19_7"></div>
			<div id="l19_0"></div>
			<div id="l19_1"></div>
			<div id="l19_2"></div>
			<div id="l19_3"></div>

			<div id="l25_c1"></div>
			<div id="l25_c2"></div>
			<div id="l25_c3"></div>
			<div id="l25_c4"></div>
		</div>

		<div id="l17" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l17_4"></div>
			<div id="l17_5"></div>
			<div id="l17_6"></div>
			<div id="l17_7"></div>
			<div id="l17_0"></div>
			<div id="l17_1"></div>
			<div id="l17_2"></div>
			<div id="l17_3"></div>
		</div>

		<div id="l15" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l15_4"></div>
			<div id="l15_5"></div>
			<div id="l15_6"></div>
			<div id="l15_7"></div>
			<div id="l15_0"></div>
			<div id="l15_1"></div>
			<div id="l15_2"></div>
			<div id="l15_3"></div>
		</div>

		<div id="l12" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l12_4"></div>
			<div id="l12_5"></div>
			<div id="l12_6"></div>
			<div id="l12_7"></div>
			<div id="l12_0"></div>
			<div id="l12_1"></div>
			<div id="l12_2"></div>
			<div id="l12_3"></div>
		</div>

		<div id="l39" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l39_3"></div>
			<div id="l39_2"></div>
			<div id="l39_1"></div>
			<div id="l39_0"></div>
			<div id="l39_7"></div>
			<div id="l39_6"></div>
			<div id="l39_5"></div>
			<div id="l39_4"></div>

			<div id="l39_c1"></div>
			<div id="l39_c2"></div>
			<div id="l39_c3"></div>
			<div id="l39_c4"></div>
		</div>

		<div id="l38" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l38_3"></div>
			<div id="l38_2"></div>
			<div id="l38_1"></div>
			<div id="l38_0"></div>
			<div id="l38_7"></div>
			<div id="l38_6"></div>
			<div id="l38_5"></div>
			<div id="l38_4"></div>
		</div>

		<div id="l37" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l37_3"></div>
			<div id="l37_2"></div>
			<div id="l37_1"></div>
			<div id="l37_0"></div>
			<div id="l37_7"></div>
			<div id="l37_6"></div>
			<div id="l37_5"></div>
			<div id="l37_4"></div>

			<div id="l37_c1"></div>
			<div id="l37_c2"></div>
			<div id="l37_c3"></div>
			<div id="l37_c4"></div>
		</div>

		<div id="l36" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l36_3"></div>
			<div id="l36_2"></div>
			<div id="l36_1"></div>
			<div id="l36_0"></div>
			<div id="l36_7"></div>
			<div id="l36_6"></div>
			<div id="l36_5"></div>
			<div id="l36_4"></div>
		</div>

		<div id="l35" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l35_3"></div>
			<div id="l35_2"></div>
			<div id="l35_1"></div>
			<div id="l35_0"></div>
			<div id="l35_7"></div>
			<div id="l35_6"></div>
			<div id="l35_5"></div>
			<div id="l35_4"></div>
		</div>

		<div id="l34" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l34_3"></div>
			<div id="l34_2"></div>
			<div id="l34_1"></div>
			<div id="l34_0"></div>
			<div id="l34_7"></div>
			<div id="l34_6"></div>
			<div id="l34_5"></div>
			<div id="l34_4"></div>

			<div id="l34_c1"></div>
			<div id="l34_c2"></div>
			<div id="l34_c3"></div>
			<div id="l34_c4"></div>
		</div>

		<div id="l33" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l33_3"></div>
			<div id="l33_2"></div>
			<div id="l33_1"></div>
			<div id="l33_0"></div>
			<div id="l33_7"></div>
			<div id="l33_6"></div>
			<div id="l33_5"></div>
			<div id="l33_4"></div>
		</div>

		<div id="l32" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l32_3"></div>
			<div id="l32_2"></div>
			<div id="l32_1"></div>
			<div id="l32_0"></div>
			<div id="l32_7"></div>
			<div id="l32_6"></div>
			<div id="l32_5"></div>
			<div id="l32_4"></div>

			<div id="l32_c1"></div>
			<div id="l32_c2"></div>
			<div id="l32_c3"></div>
			<div id="l32_c4"></div>
		</div>

		<div id="l31" onclick="getInfoLotto(this.id, <%=idPartita%>)">
			<div id="l31_3"></div>
			<div id="l31_7"></div>
			<div id="l31_2"></div>
			<div id="l31_1"></div>
			<div id="l31_0"></div>
			<div id="l31_6"></div>
			<div id="l31_5"></div>
			<div id="l31_4"></div>

			<div id="l31_c1"></div>
			<div id="l31_c2"></div>
			<div id="l31_c3"></div>
			<div id="l31_c4"></div>
		</div>

	</div>
	<div class="gui">
		<div id="infopartita"></div>							
		<div id="infogiocatore"></div>
		<button id="ft" onclick="fineTurno(<%=idGioc%>)">fine turno</button>
		<button id="cc" onclick="costruisciCasa(<%=idGioc%>)">costruisci casa</button>
		<button id="ca" onclick="costruisciAlbergo(<%=idGioc%>)">costruiscialbergo</button>
		<button id="cct" onclick="compraContratto(<%=idGioc%>)">compra contratto</button>
		<button id="vcd" onclick="vendiContratto(<%=idGioc%>)">vendi contratto</button>
		<button id="ld" onclick="lanciaDadi(<%=idGioc%>)">lancia dadi</button>
		<div id="infolotto"></div>

	</div>





</body>
</html>