CREATE DATABASE  IF NOT EXISTS `monopolyfinale` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `monopolyfinale`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: monopolyfinale
-- ------------------------------------------------------
-- Server version	5.1.73-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `giocatore`
--

DROP TABLE IF EXISTS `giocatore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `giocatore` (
  `idGiocatore` int(1) NOT NULL AUTO_INCREMENT,
  `idPartita` int(1) DEFAULT NULL,
  `nomeGiocatore` varchar(100) NOT NULL,
  `nomeIcona` varchar(100) NOT NULL,
  `posizionePedina` int(1) NOT NULL DEFAULT '0',
  `turno` int(1) NOT NULL DEFAULT '0',
  `soldi` int(1) NOT NULL DEFAULT '1500',
  `carteUscitaPrigione` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idGiocatore`),
  KEY `idPartita_idx` (`idPartita`),
  CONSTRAINT `idPartita` FOREIGN KEY (`idPartita`) REFERENCES `par` (`idPartita`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='sel la partita non esiste --> creare prima giocatore lascian';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `giocatore`
--

LOCK TABLES `giocatore` WRITE;
/*!40000 ALTER TABLE `giocatore` DISABLE KEYS */;
INSERT INTO `giocatore` VALUES (1,NULL,'andrea','gg',5,0,1500,0),(2,NULL,'massimo','r4r',0,0,1500,0),(3,NULL,'paolo','jfie',0,0,1500,0),(4,NULL,'giuseppe','fvsfvs',0,0,1500,0);
/*!40000 ALTER TABLE `giocatore` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `monopolyfinale`.`giocatore_BEFORE_INSERT` BEFORE INSERT ON `giocatore` FOR EACH ROW
BEGIN
	/*posizione pedina fuori dalla griglia*/
	if ((new.posizionePedina > 39) or (new.posizionePedina < 0)) then
		set new.posizionePedina = 0;
	end if;
    
    /* turno deve valere solo 0 o 1*/
    if (not (new.turno=0) or (new.turno=1)) then
		set new.turno = 0;
    end if; 
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `monopolyfinale`.`giocatore_AFTER_INSERT` AFTER INSERT ON `giocatore` FOR EACH ROW
BEGIN
	/*aggiornamento numero di giocatori all'interno della sessione di gioco*/
    if new.idPartita != null then
		update partita p
		set numeroGiocatori = ( select count(*)
							    from giocatore g
                                where g.idPartita = p.idPartita);
	end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `monopolyfinale`.`giocatore_BEFORE_UPDATE` BEFORE UPDATE ON `giocatore` FOR EACH ROW
BEGIN
	/*posizione pedina fuori dalla griglia*/
	if (new.posizionePedina > 39) or (new.posizionePedina < 0) then
		set new.posizionePedina = 0;
	end if;
    
    /* turno deve valere solo 0 o 1*/
    if (not (new.turno=0) or (new.turno=1)) then
		set new.turno = 0;
    end if; 
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `monopolyfinale`.`giocatore_AFTER_UPDATE` AFTER UPDATE ON `giocatore` FOR EACH ROW
BEGIN
/*aggiornamento numero di giocatori all'interno della sessione di gioco*/
/*	update partita p
	set numeroGiocatoriCorrenti = 1+( select count(*)
									  from giocatore g
									  where g.idPartita = new.idPartita)
	where p.idPartita = new.idPartita;*/
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `monopolyfinale`.`giocatore_AFTER_DELETE` AFTER DELETE ON `giocatore` FOR EACH ROW
BEGIN
/*aggiornamento numero giocatori partita*/
		if(old.nomeGiocatore != null) then
			update partita p
			set numeroGiocatoriCorrenti = ( select count(*)
											from giocatore g
											where g.idPartita = p.idPartita)
			where p.idPartita = old.idPartita; 
		end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `imprevisto`
--

DROP TABLE IF EXISTS `imprevisto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imprevisto` (
  `idImprevisto` int(1) NOT NULL,
  `testo` varchar(500) NOT NULL,
  `credito` int(1) unsigned zerofill NOT NULL,
  `debito` int(1) unsigned zerofill NOT NULL,
  `spostamento` int(1) DEFAULT NULL,
  `uscita di prigione` int(1) unsigned zerofill NOT NULL,
  PRIMARY KEY (`idImprevisto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imprevisto`
--

LOCK TABLES `imprevisto` WRITE;
/*!40000 ALTER TABLE `imprevisto` DISABLE KEYS */;
INSERT INTO `imprevisto` VALUES (0,'Andate sino al Largo Colombo: se passate dal \"Via\" ritirate  500 euro',500,0,24,0),(1,'Fate tre passi indietro',0,0,-3,0),(2,'Maturano le cedole delle vostre cartelle di rendita, ritirate  375 euro',375,0,NULL,0),(3,'Andate avanti sino al \"Via\"',0,0,0,0),(4,'Avete vinto un terno al lotto: ritirate  250 euro',250,0,NULL,0),(5,'Uscite gratis di prigione, se ci siete: potete conservare questo cartoncino sino al momento di servirvene',0,0,NULL,1),(6,'La Banca vi paga gli interessi del vostro Conto Corrente, ritirate 125 euro',125,0,NULL,0),(7,'Matrimonio in famiglia: spese impreviste 375 euro',500,375,NULL,0),(8,'Andate alla stazione Nord: se passate dal \"Via\" ritirate  500 euro',500,0,25,0),(9,'Andate sino a Via Accademia: se passate dal \"Via\" ritirate  500 euro',0,0,11,0),(10,'Andate fino al Parco della Vittoria',0,0,39,0),(11,'Multa di € 40 per aver guidato senza patente',0,40,NULL,0),(12,'Versate € 50 per beneficienza',0,50,NULL,0);
/*!40000 ALTER TABLE `imprevisto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lotto`
--

DROP TABLE IF EXISTS `lotto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lotto` (
  `idlotto` int(1) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `colore` varchar(10) DEFAULT NULL,
  `costoContratto` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  `rendita0` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  `rendita1` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  `rendita2` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  `rendita3` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  `rendita5` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  `rendita6` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  `renditaSpeciale` int(1) NOT NULL DEFAULT '0',
  `costoCasa` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  `costoAlbergo` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  PRIMARY KEY (`idlotto`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lotto`
--

LOCK TABLES `lotto` WRITE;
/*!40000 ALTER TABLE `lotto` DISABLE KEYS */;
INSERT INTO `lotto` VALUES (0,'Via',NULL,0,0,0,0,0,0,0,0,0,0),(1,'Vicolo Corto','rosa',150,5,25,75,225,400,625,0,125,125),(2,'Probabilita',NULL,0,0,0,0,0,0,0,0,0,0),(3,'Vicolo Stretto','rosa',150,10,50,150,450,800,1125,0,125,125),(4,'Tassa Patrimoniale',NULL,0,0,0,0,0,0,0,0,0,0),(5,'Stazione Sud','stazione',180,60,120,240,480,0,0,0,0,0),(6,'Bastioni Gran Sasso','blu',250,15,75,225,675,1000,1375,0,125,125),(7,'Imprevisti',NULL,0,0,0,0,0,0,0,0,0,0),(8,'Viale Monterosa','blu',250,15,75,225,675,1000,1375,0,125,125),(9,'Viale Vesuvio','blu',300,20,100,250,750,1125,1500,0,125,125),(10,'Prigione',NULL,0,0,0,0,0,0,0,0,0,0),(11,'Via Accademia','arancione',350,25,125,375,1125,1550,1875,0,250,250),(12,'Societa Elettrica','societa',380,50,0,0,0,0,0,0,0,0),(13,'Corso Ateneo','arancione',350,25,125,375,1125,1550,1875,0,250,250),(14,'Piazza Universita','arancione',400,30,150,450,1250,1750,2250,0,250,250),(15,'Stazione Ovest','stazione',480,60,120,240,480,0,0,0,0,0),(16,'Via Verdi','marrone',450,35,175,500,1375,1875,2375,0,250,250),(17,'Probabilita',NULL,0,0,0,0,0,0,0,0,0,0),(18,'Corso Raffaello','marrone',450,35,175,500,1375,1875,2375,0,250,250),(19,'Piazza Dante','marrone',500,40,200,550,1500,2000,2500,0,250,250),(20,'Posteggio',NULL,0,0,0,0,0,0,0,0,0,0),(21,'Via Marco Polo','rosso',550,45,225,625,1750,2200,2625,0,375,375),(22,'Imprevisti',NULL,0,0,0,0,0,0,0,0,0,0),(23,'Corso Magellano','rosso',550,45,225,625,1750,2200,2625,0,375,375),(24,'Largo Colombo','rosso',600,50,250,750,1875,2250,2750,0,375,375),(25,'Stazione sud','stazione',180,60,120,240,480,0,0,0,0,0),(26,'Viale Costantino','giallo',650,55,275,825,2000,2500,3000,0,375,375),(27,'Viale Traiano','giallo',650,55,275,825,2000,2500,3000,0,375,375),(28,'Societa Acqua Potabile','societa',0,50,0,0,0,0,0,0,0,0),(29,'Piazza Giulio Cesare','giallo',700,60,300,900,2125,2625,3125,0,375,375),(30,'Vai in Prigione',NULL,0,0,0,0,0,0,0,0,0,0),(31,'Via Roma','verde',750,65,325,1000,2250,2750,3250,0,500,500),(32,'Corso Impero','verde',750,65,325,1000,2250,2750,3250,0,500,500),(33,'Probabilita',NULL,0,0,0,0,0,0,0,0,0,0),(34,'Largo Augusto','verde',800,70,375,1125,2500,3000,3500,0,500,500),(35,'Stazione Est','stazione',480,60,120,240,480,0,0,0,0,0),(36,'Imprevisti',NULL,0,0,0,0,0,0,0,0,0,0),(37,'Viale dei Giardini','viola',900,90,500,1250,2750,3250,3750,0,500,500),(38,'tassa di lusso',NULL,0,0,0,0,0,0,0,0,0,0),(39,'Parco della Vittoria','viola',1000,125,500,1500,3500,4250,5000,0,500,500);
/*!40000 ALTER TABLE `lotto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partita`
--

DROP TABLE IF EXISTS `partita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partita` (
  `idPartita` int(1) NOT NULL AUTO_INCREMENT,
  `idProprietario` int(1) NOT NULL,
  `nomePartita` varchar(100) NOT NULL,
  `numeroMaxGiocatori` int(1) NOT NULL DEFAULT '8',
  `numeroGiocatoriCorrenti` int(1) NOT NULL DEFAULT '1',
  `partitaIniziata` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idPartita`),
  KEY `idPropietario_idx` (`idProprietario`),
  CONSTRAINT `idPropietario` FOREIGN KEY (`idProprietario`) REFERENCES `giocatore` (`idGiocatore`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partita`
--

LOCK TABLES `partita` WRITE;
/*!40000 ALTER TABLE `partita` DISABLE KEYS */;
INSERT INTO `partita` VALUES (2,1,'dd',8,1,0);
/*!40000 ALTER TABLE `partita` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `monopolyfinale`.`partita_BEFORE_INSERT` BEFORE INSERT ON `partita` FOR EACH ROW
BEGIN
	/*CONTROLLO NUMERO MASSIMO DI GIOCATORI*/
	if new.numeroMaxGiocatori>8 then
		SET new.numeroMaxGiocatori=8;
    END IF;
    
    /*CONTROLLO GICATORI CORRENTI*/
    if new.numeroGiocatoriCorrenti> new.numeroMaxGIocatori then
		SET new.numeroGiocatoriCorrenti=8;
    END IF;
    
    /*CONTROLLO STATO INIZIO PARTITA*/
    if (not((new.partitaIniziata = 0) or (new.partitaIniziata = 1))) then
		set new.partitaIniziata = 0;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `monopolyfinale`.`partita_AFTER_INSERT` AFTER INSERT ON `partita` FOR EACH ROW
BEGIN
/*AGGIORNAMENTO GIOCATORE CHE CREA LA PARTITA*/
 UPDATE giocatore
 set idPartita=new.idPartita
 WHERE idGiocatore = new.idProprietario;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `monopolyfinale`.`partita_BEFORE_UPDATE` BEFORE UPDATE ON `partita` FOR EACH ROW
BEGIN
	/*CONTROLLO NUMERO MASSIMO DI GIOCATORI*/
	if new.numeroMaxGiocatori>8 then
		SET new.numeroMaxGiocatori=8;
    END IF;
    
    /*CONTROLLO GICATORI CORRENTI*/
    if new.numeroGiocatoriCorrenti> new.numeroMaxGIocatori then
		SET new.numeroGiocatoriCorrenti=8;
    END IF;
    
    /*CONTROLLO STATO INIZIO PARTITA*/
    if (not((new.partitaIniziata = 0) or (new.partitaIniziata = 1))) then
		set new.partitaIniziata = 0;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `monopolyfinale`.`partita_BEFORE_DELETE` BEFORE DELETE ON `partita` FOR EACH ROW
BEGIN
/*ELIMINA TUTTI I GIOCATORI ASSOCIATI ALLA SESSIONE DI GIOCO*/
	delete from giocatore
    where idPartita = old.idPartita;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `partitalottogiocatore`
--

DROP TABLE IF EXISTS `partitalottogiocatore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partitalottogiocatore` (
  `idPartitaLottoGiocatore` int(11) NOT NULL AUTO_INCREMENT,
  `idGiocatore` int(11) NOT NULL,
  `idPartita` int(11) NOT NULL,
  `idLotto` int(11) NOT NULL,
  `nCase` int(11) NOT NULL DEFAULT '0',
  `nAlberghi` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idPartitaLottoGiocatore`),
  KEY `idLotto_idx` (`idLotto`),
  KEY `idGIocatore_idx` (`idGiocatore`),
  KEY `idPartita_idx` (`idPartita`),
  CONSTRAINT `idGIocatore` FOREIGN KEY (`idGiocatore`) REFERENCES `giocatore` (`idGiocatore`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `idLotto` FOREIGN KEY (`idLotto`) REFERENCES `lotto` (`idlotto`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partitalottogiocatore`
--

LOCK TABLES `partitalottogiocatore` WRITE;
/*!40000 ALTER TABLE `partitalottogiocatore` DISABLE KEYS */;
/*!40000 ALTER TABLE `partitalottogiocatore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `probabilita`
--

DROP TABLE IF EXISTS `probabilita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `probabilita` (
  `idprobabilita` int(1) NOT NULL,
  `testo` varchar(1000) NOT NULL,
  `credito` int(1) unsigned zerofill DEFAULT NULL,
  `debito` int(1) unsigned zerofill DEFAULT NULL,
  `spostamento` int(1) DEFAULT NULL,
  PRIMARY KEY (`idprobabilita`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `probabilita`
--

LOCK TABLES `probabilita` WRITE;
/*!40000 ALTER TABLE `probabilita` DISABLE KEYS */;
INSERT INTO `probabilita` VALUES (0,'Avete venduto delle azioni: ricavate 125 euro',125,0,NULL),(1,'Andate fino al \"Via\"',0,0,0),(2,'Scade il vostro premio di assicurazione: pagate 125 euro',0,125,NULL),(3,'Avete perso una causa: pagate 250 euro',0,250,NULL),(4,'Rimborso tassa sul reddito: ritirate 50 euro dalla Banca',50,0,NULL),(5,'E\' maturata la cedola delle vostre azioni: ritirate 60 euro',60,0,NULL),(6,'Siete creditori verso la Banca di 500 euro: ritirateli',500,0,NULL),(7,'Avete vinto un premio di consolazione alla Lotteria di Merano: ritirate 250 euro',250,0,NULL),(8,'Pagate il conto del Dottore: 125 euro',125,0,NULL),(9,'Avete vinto il secondo premio di un concorso di bellezza: ritirate  25 euro',25,0,NULL),(10,'Uscite gratis di prigione, se ci siete: potete conservare questo cartoncino sino al momento di servirvene',0,0,NULL),(11,'Ritornate al Vicolo Corto',0,0,1),(12,'Pagate una multa di 25 euro',0,25,NULL),(13,'E\' il vostro compleanno: la banca vi regala 25 euro',25,0,NULL),(14,'Ereditate da un lontano parente 250 euro',250,0,NULL),(15,'Andate in prigione direttamente e senza passare dal \"Via\"',0,0,10);
/*!40000 ALTER TABLE `probabilita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'monopolyfinale'
--

--
-- Dumping routines for database 'monopolyfinale'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-09  9:19:26
