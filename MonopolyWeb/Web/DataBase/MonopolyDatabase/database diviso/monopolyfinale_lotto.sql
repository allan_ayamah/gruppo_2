CREATE DATABASE  IF NOT EXISTS `monopolyfinale` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `monopolyfinale`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: monopolyfinale
-- ------------------------------------------------------
-- Server version	5.1.73-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lotto`
--

DROP TABLE IF EXISTS `lotto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lotto` (
  `idlotto` int(1) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `colore` varchar(10) DEFAULT NULL,
  `costoContratto` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  `rendita0` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  `rendita1` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  `rendita2` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  `rendita3` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  `rendita5` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  `rendita6` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  `renditaSpeciale` int(1) NOT NULL DEFAULT '0',
  `costoCasa` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  `costoAlbergo` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  PRIMARY KEY (`idlotto`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lotto`
--

LOCK TABLES `lotto` WRITE;
/*!40000 ALTER TABLE `lotto` DISABLE KEYS */;
INSERT INTO `lotto` VALUES (0,'Via',NULL,0,0,0,0,0,0,0,0,0,0),(1,'Vicolo Corto','rosa',150,5,25,75,225,400,625,0,125,125),(2,'Probabilita',NULL,0,0,0,0,0,0,0,0,0,0),(3,'Vicolo Stretto','rosa',150,10,50,150,450,800,1125,0,125,125),(4,'Tassa Patrimoniale',NULL,0,0,0,0,0,0,0,0,0,0),(5,'Stazione Sud','stazione',180,60,120,240,480,0,0,0,0,0),(6,'Bastioni Gran Sasso','blu',250,15,75,225,675,1000,1375,0,125,125),(7,'Imprevisti',NULL,0,0,0,0,0,0,0,0,0,0),(8,'Viale Monterosa','blu',250,15,75,225,675,1000,1375,0,125,125),(9,'Viale Vesuvio','blu',300,20,100,250,750,1125,1500,0,125,125),(10,'Prigione',NULL,0,0,0,0,0,0,0,0,0,0),(11,'Via Accademia','arancione',350,25,125,375,1125,1550,1875,0,250,250),(12,'Societa Elettrica','societa',380,50,0,0,0,0,0,0,0,0),(13,'Corso Ateneo','arancione',350,25,125,375,1125,1550,1875,0,250,250),(14,'Piazza Universita','arancione',400,30,150,450,1250,1750,2250,0,250,250),(15,'Stazione Ovest','stazione',480,60,120,240,480,0,0,0,0,0),(16,'Via Verdi','marrone',450,35,175,500,1375,1875,2375,0,250,250),(17,'Probabilita',NULL,0,0,0,0,0,0,0,0,0,0),(18,'Corso Raffaello','marrone',450,35,175,500,1375,1875,2375,0,250,250),(19,'Piazza Dante','marrone',500,40,200,550,1500,2000,2500,0,250,250),(20,'Posteggio',NULL,0,0,0,0,0,0,0,0,0,0),(21,'Via Marco Polo','rosso',550,45,225,625,1750,2200,2625,0,375,375),(22,'Imprevisti',NULL,0,0,0,0,0,0,0,0,0,0),(23,'Corso Magellano','rosso',550,45,225,625,1750,2200,2625,0,375,375),(24,'Largo Colombo','rosso',600,50,250,750,1875,2250,2750,0,375,375),(25,'Stazione sud','stazione',180,60,120,240,480,0,0,0,0,0),(26,'Viale Costantino','giallo',650,55,275,825,2000,2500,3000,0,375,375),(27,'Viale Traiano','giallo',650,55,275,825,2000,2500,3000,0,375,375),(28,'Societa Acqua Potabile','societa',0,50,0,0,0,0,0,0,0,0),(29,'Piazza Giulio Cesare','giallo',700,60,300,900,2125,2625,3125,0,375,375),(30,'Vai in Prigione',NULL,0,0,0,0,0,0,0,0,0,0),(31,'Via Roma','verde',750,65,325,1000,2250,2750,3250,0,500,500),(32,'Corso Impero','verde',750,65,325,1000,2250,2750,3250,0,500,500),(33,'Probabilita',NULL,0,0,0,0,0,0,0,0,0,0),(34,'Largo Augusto','verde',800,70,375,1125,2500,3000,3500,0,500,500),(35,'Stazione Est','stazione',480,60,120,240,480,0,0,0,0,0),(36,'Imprevisti',NULL,0,0,0,0,0,0,0,0,0,0),(37,'Viale dei Giardini','viola',900,90,500,1250,2750,3250,3750,0,500,500),(38,'tassa di lusso',NULL,0,0,0,0,0,0,0,0,0,0),(39,'Parco della Vittoria','viola',1000,125,500,1500,3500,4250,5000,0,500,500);
/*!40000 ALTER TABLE `lotto` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-09  7:28:58
