CREATE DATABASE  IF NOT EXISTS `monopolyfinale` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `monopolyfinale`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: monopolyfinale
-- ------------------------------------------------------
-- Server version	5.1.73-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `probabilita`
--

DROP TABLE IF EXISTS `probabilita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `probabilita` (
  `idprobabilita` int(1) NOT NULL,
  `testo` varchar(1000) NOT NULL,
  `credito` int(1) unsigned zerofill DEFAULT NULL,
  `debito` int(1) unsigned zerofill DEFAULT NULL,
  `spostamento` int(1) DEFAULT NULL,
  PRIMARY KEY (`idprobabilita`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `probabilita`
--

LOCK TABLES `probabilita` WRITE;
/*!40000 ALTER TABLE `probabilita` DISABLE KEYS */;
INSERT INTO `probabilita` VALUES (0,'Avete venduto delle azioni: ricavate 125 euro',125,0,NULL),(1,'Andate fino al \"Via\"',0,0,0),(2,'Scade il vostro premio di assicurazione: pagate 125 euro',0,125,NULL),(3,'Avete perso una causa: pagate 250 euro',0,250,NULL),(4,'Rimborso tassa sul reddito: ritirate 50 euro dalla Banca',50,0,NULL),(5,'E\' maturata la cedola delle vostre azioni: ritirate 60 euro',60,0,NULL),(6,'Siete creditori verso la Banca di 500 euro: ritirateli',500,0,NULL),(7,'Avete vinto un premio di consolazione alla Lotteria di Merano: ritirate 250 euro',250,0,NULL),(8,'Pagate il conto del Dottore: 125 euro',125,0,NULL),(9,'Avete vinto il secondo premio di un concorso di bellezza: ritirate  25 euro',25,0,NULL),(10,'Uscite gratis di prigione, se ci siete: potete conservare questo cartoncino sino al momento di servirvene',0,0,NULL),(11,'Ritornate al Vicolo Corto',0,0,1),(12,'Pagate una multa di 25 euro',0,25,NULL),(13,'E\' il vostro compleanno: la banca vi regala 25 euro',25,0,NULL),(14,'Ereditate da un lontano parente 250 euro',250,0,NULL),(15,'Andate in prigione direttamente e senza passare dal \"Via\"',0,0,10);
/*!40000 ALTER TABLE `probabilita` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-09  7:28:57
