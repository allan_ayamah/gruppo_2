CREATE DATABASE  IF NOT EXISTS `monopolyfinale` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `monopolyfinale`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: monopolyfinale
-- ------------------------------------------------------
-- Server version	5.1.73-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `imprevisto`
--

DROP TABLE IF EXISTS `imprevisto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imprevisto` (
  `idImprevisto` int(1) NOT NULL,
  `testo` varchar(500) NOT NULL,
  `credito` int(1) unsigned zerofill NOT NULL,
  `debito` int(1) unsigned zerofill NOT NULL,
  `spostamento` int(1) DEFAULT NULL,
  `uscita di prigione` int(1) unsigned zerofill NOT NULL,
  PRIMARY KEY (`idImprevisto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imprevisto`
--

LOCK TABLES `imprevisto` WRITE;
/*!40000 ALTER TABLE `imprevisto` DISABLE KEYS */;
INSERT INTO `imprevisto` VALUES (0,'Andate sino al Largo Colombo: se passate dal \"Via\" ritirate  500 euro',500,0,24,0),(1,'Fate tre passi indietro',0,0,-3,0),(2,'Maturano le cedole delle vostre cartelle di rendita, ritirate  375 euro',375,0,NULL,0),(3,'Andate avanti sino al \"Via\"',0,0,0,0),(4,'Avete vinto un terno al lotto: ritirate  250 euro',250,0,NULL,0),(5,'Uscite gratis di prigione, se ci siete: potete conservare questo cartoncino sino al momento di servirvene',0,0,NULL,1),(6,'La Banca vi paga gli interessi del vostro Conto Corrente, ritirate 125 euro',125,0,NULL,0),(7,'Matrimonio in famiglia: spese impreviste 375 euro',500,375,NULL,0),(8,'Andate alla stazione Nord: se passate dal \"Via\" ritirate  500 euro',500,0,25,0),(9,'Andate sino a Via Accademia: se passate dal \"Via\" ritirate  500 euro',0,0,11,0),(10,'Andate fino al Parco della Vittoria',0,0,39,0),(11,'Multa di € 40 per aver guidato senza patente',0,40,NULL,0),(12,'Versate € 50 per beneficienza',0,50,NULL,0);
/*!40000 ALTER TABLE `imprevisto` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-09  7:28:58
