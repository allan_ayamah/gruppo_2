CREATE DATABASE  IF NOT EXISTS `monopolyfinale` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `monopolyfinale`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: monopolyfinale
-- ------------------------------------------------------
-- Server version	5.1.73-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `partitalottogiocatore`
--

DROP TABLE IF EXISTS `partitalottogiocatore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partitalottogiocatore` (
  `idPartitaLottoGiocatore` int(11) NOT NULL AUTO_INCREMENT,
  `idGiocatore` int(11) NOT NULL,
  `idPartita` int(11) NOT NULL,
  `idLotto` int(11) NOT NULL,
  `nCase` int(11) NOT NULL DEFAULT '0',
  `nAlberghi` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idPartitaLottoGiocatore`),
  KEY `idLotto_idx` (`idLotto`),
  KEY `idGIocatore_idx` (`idGiocatore`),
  KEY `idPartita_idx` (`idPartita`),
  CONSTRAINT `idGIocatore` FOREIGN KEY (`idGiocatore`) REFERENCES `giocatore` (`idGiocatore`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `idLotto` FOREIGN KEY (`idLotto`) REFERENCES `lotto` (`idlotto`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partitalottogiocatore`
--

LOCK TABLES `partitalottogiocatore` WRITE;
/*!40000 ALTER TABLE `partitalottogiocatore` DISABLE KEYS */;
/*!40000 ALTER TABLE `partitalottogiocatore` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-09  7:28:57
