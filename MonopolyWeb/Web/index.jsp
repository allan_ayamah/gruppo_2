<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<link rel="icon" href="image/favicon.ico" />
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>


<link rel="styleSheet" href="css/index.css">
<script type="text/javascript" src="js/index.js"></script>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Monopoly: Home page</title>
</head>
<body onload="caricaTabPartite('tabellaPartite')">

	<!-- ***Div messaggi di errore*** -->
	<%if(request.getParameter("err") != null){ %>
	<div class="alert alert-danger text-center">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>ERROR!</strong> <%= request.getParameter("err") %>
	</div>
	<%} %>
	
<div class="col-md-12">
	<!-- ***Div contenitore dei dati della form*** -->
	<div id="containerDati" class="well col-md-7">

		<!-- ***Div contenitore logo*** -->
		<div class="col-md-12 text-center">
			<img id="logo" src="image/monopolyLogo.png" />
		</div>
		
		
		<div class="panel-group col-md-12" id="panelGroup">
		
			<!-- ***PANNELLO CONTENENTE LA CREAZIONE DELLA SESSIONE DI GIOCO*** -->
			<div id="pannello" class="panel panel-default">
				<div id="intestazionePannello" class="panel-heading">
					<h4 class="panel-title" data-toggle="collapse" data-parent="#panelGroup" href="#menuCreaPartita">Crea una nuova sessione di gioco</h4>
				</div>
				<div id="menuCreaPartita" class="panel-collapse collapse">
					<div class="panel-body">
					
						<!--  ***FORM CREAZIONE PARTIRA*** -->
						<form method="get" id="formCreaPartita" action="creapartita">
						
							<!--  ***INSERISCI UTENTE*** -->
							<div class="form-group">
								<label>Inserisci username:</label> 
								<input type="text" class="form-control" id="username" name="username">
							</div>
							
							<!--  ***SCEGLI PEDINA*** -->
							<div class="form-group">
								<input class="invisibile" type="text" id="pedinaIco" name="pedinaIco"/>							<!-- input "pedinaIco" memorizza il nome della pedina -->
								<label>Scegli una pedina:</label>
								<div>
									<div id="listaGlyphicon" class="col-md-12">
										<div id="ico0" class="glyphicon glyphicon-piggy-bank col-md-3" onclick="selezionaIcona(this,'0','formCreaPartita')"></div>
										<div id="ico1" class="glyphicon glyphicon-apple col-md-3" onclick="selezionaIcona(this,'1','formCreaPartita')"></div>
										<div id="ico2" class="glyphicon glyphicon-baby-formula col-md-3" onclick="selezionaIcona(this,'2','formCreaPartita')"></div>
										<div id="ico3" class="glyphicon glyphicon-king col-md-3" onclick="selezionaIcona(this,'3','formCreaPartita')"></div>
									</div>
									<div id="listaGlyphicon" class="col-md-12">
										<div id="ico4" class="glyphicon glyphicon-tree-conifer col-md-3" onclick="selezionaIcona(this,'4','formCreaPartita')"></div>
										<div id="ico5" class="glyphicon glyphicon-leaf col-md-3" onclick="selezionaIcona(this,'5','formCreaPartita')"></div>
										<div id="ico6" class="glyphicon glyphicon-fire col-md-3" onclick="selezionaIcona(this,'6','formCreaPartita')"></div>
										<div id="ico7" class="glyphicon glyphicon-gift col-md-3" onclick="selezionaIcona(this,'7','formCreaPartita')"></div>
									</div>
								</div>
							</div>
							
							<!-- *** INSERIMENTO NOME DELLA PARTITA*** -->
							<div class="form-group">
								<label>Inserisci nome della partita:</label>
								<input type="text" class="form-control" id="nomePartita" name="nomePartita">
							</div>
							
							<!-- *** INSERIMENTO NUMERO MASSIMO DI GIOCATORI*** -->
							<div class="form-group">
								<label>Inserisci numero massimo di giocatori:</label> 
								<select class="form-control" id="numGiocatori" name="numGiocatori">
									<%
										for (int i = 2; i <= 8; i++) {
											if (i != 8) {
									%>
									<option><%=i%></option>
									<%
										} else {
									%>
									<option selected="selected"><%=i%></option>
									<%
										}
									%>
									<%
										}
									%>
								</select>
							</div>
							
							<!-- ***BOTTONE SUBMIT*** -->
							<input class="col-md-12 btn btn-primary  btn-lg  text-center" type="submit" value="Invia">
						</form>
					</div>
				</div>
			</div>

			<!--PANNELLO CONTENENTE L'UNIONE AD UNA PARTITA GIA' ESISTENTE -->
			<div id="pannello" class="panel panel-default">
				<div id="intestazionePannello" class="panel-heading">
					<h4 class="panel-title" data-toggle="collapse" data-parent="#panelGroup" href="#menuUnisciPartita">Unisciti ad una partita esistente</h4>
				</div>
				<div id="menuUnisciPartita" class="panel-collapse collapse">
					<div class="panel-body">
					
						<!--  ***FORM UNISCITI ALLA PARTITA*** -->
						<form method="get" id="formUnisciPartita" action="uniscipartita">
							
							<!--  ***INSERISCI UTENTE*** -->
							<div class="form-group">
								<label>Inserisci username:</label> 
								<input type="text" class="form-control" id="username" name="username">
							</div>
							
							<!--  ***SCEGLI PEDINA*** -->
							<div class="form-group">
								<input class="invisibile" type="text" id="pedinaIco" name="pedinaIco"/>
								<label>Scegli una pedina:</label>
								<div>
									<div id="listaGlyphicon"class="col-md-12">
										<div id="ico0" class="glyphicon glyphicon-piggy-bank col-md-3" onclick="selezionaIcona(this,'glyphicon glyphicon-piggy-bank','formUnisciPartita')"></div>
										<div id="ico1" class="glyphicon glyphicon-apple col-md-3" onclick="selezionaIcona(this,'glyphicon glyphicon-apple','formUnisciPartita')"></div>
										<div id="ico2" class="glyphicon glyphicon-baby-formula col-md-3" onclick="selezionaIcona(this,'glyphicon glyphicon-baby-formula','formUnisciPartita')"></div>
										<div id="ico3" class="glyphicon glyphicon-king col-md-3" onclick="selezionaIcona(this,'glyphicon glyphicon-king','formUnisciPartita')"></div>
									</div>
									<div id="listaGlyphicon" class="col-md-12">
										<div id="ico4" class="glyphicon glyphicon-tree-conifer col-md-3" onclick="selezionaIcona(this,'glyphicon-tree-conifer','formUnisciPartita')"></div>
										<div id="ico5" class="glyphicon glyphicon-leaf col-md-3" onclick="selezionaIcona(this,'glyphicon-leaf','formUnisciPartita')"></div>
										<div id="ico6" class="glyphicon glyphicon-fire col-md-3" onclick="selezionaIcona(this,'glyphicon glyphicon-fire','formUnisciPartita')"></div>
										<div id="ico7" class="glyphicon glyphicon-gift col-md-3" onclick="selezionaIcona(this,'glyphicon glyphicon-gift','formUnisciPartita')"></div>
									</div>
								</div>
							</div>					

							<!--  ***TABELLA CONTENENTE TUTTE LE PARTITE*** -->
							<div class="form-group">
								<label>Seleziona una partita:</label>
								<input class="invisibile" type="text" id="idPartita" name="idPartita"/>	
								<div id="tabellaPartite" class="container col-md-12" name="tabellaPartita"></div>

							</div>
							
							<!-- ***BOTTONE SUBMIT*** -->
							<input class="col-md-12 btn btn-primary  btn-lg text-center" type="submit" value="Invia" >
						</form>
					</div>
				</div>
			</div>
		</div>
</div>
</body>
</html>