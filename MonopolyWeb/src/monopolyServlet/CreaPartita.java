package monopolyServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.mysql.jdbc.Connection;

/**
 * Servlet implementation class CreaPartita
 */
public class CreaPartita extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if((request.getParameter("username") != null) && (request.getParameter("pedinaIco")!= null) && (request.getParameter("nomePartita")!= null) && (request.getParameter("numGiocatori")!= null)){
			try {
				Connection con = (Connection) DbUtils.getMySqlConnection();
				DbUtils.setCreatorePartita(con, request.getParameter("username"), request.getParameter("pedinaIco"));									//crea propietario
				Integer idGiocatore = DbUtils.getIdGioc(con, request.getParameter("username"), request.getParameter("pedinaIco")); 						//rilevare id giocatore
				DbUtils.createPartita(con, idGiocatore.toString(), request.getParameter("nomePartita"), request.getParameter("numGiocatori"), "1");		//creazione partita
				Integer idPartita = DbUtils.getIdPart(con, idGiocatore.toString());
				
				//cookies
				HttpSession s = request.getSession(true);
				s.setAttribute("idGiocatore",idGiocatore.toString());
				s.setAttribute("idPartita",idPartita.toString());
				response.sendRedirect("http://localhost:8080/MonopolyWeb/CampoGioco.jsp");
						
			} catch (ClassNotFoundException | SQLException e) {
				response.sendRedirect("http://localhost:8080/MonopolyWeb/index.jsp?err=ci sono dei problemi con il server!!!!!");
			}
			
		}else{
			response.sendRedirect("http://localhost:8080/MonopolyWeb/index.jsp?err=non hai compilato correttamente tutti i campi!!!!!");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
