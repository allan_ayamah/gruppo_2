package monopolyServlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.Connection;

/**
 * Servlet implementation class UnisciPartita
 */
@WebServlet("/UnisciPartita")
public class UnisciPartita extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if((request.getParameter("username") != null) && (request.getParameter("pedinaIco") != null) && (request.getParameter("idPartita") != null)){
			try {
				String nomeGioc = request.getParameter("username");
				String icona = request.getParameter("pedinaIco");
				String idPart = request.getParameter("idPartita");

				Connection con = (Connection) DbUtils.getMySqlConnection();			//connessione database
				DbUtils.setGiocatore(con, nomeGioc, icona, idPart);					//creazione giocatore
				Integer idGiocatore = DbUtils.getIdGioc(con, request.getParameter("username"), request.getParameter("pedinaIco")); 	
				
				//cookies
				HttpSession s = request.getSession(true);
				s.setAttribute("idGiocatore",idGiocatore.toString());
				s.setAttribute("idPartita",idPart.toString());
				response.sendRedirect("http://localhost:8080/MonopolyWeb/CampoGioco.jsp");
				
			} catch (ClassNotFoundException | SQLException e){
				response.sendRedirect("http://localhost:8080/MonopolyWeb/index.jsp?err=ci sono dei problemi con il server!!!!!");
			}

		}else{
			response.sendRedirect("http://localhost:8080/MonopolyWeb/index.jsp?err=non hai compilato correttamente tutti i campi!!!!!");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
