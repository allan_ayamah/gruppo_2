//esegue query sql e restituisce il risutato sotto forma di XML
package monopolyServlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;


public class Query extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/xml;charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		
		ResultSet rs = null;
		Connection conn = null;
		
		if((request.getParameter("idQuery") != null) && (!request.getParameter("idQuery").equals(""))){
			String idQuery= request.getParameter("idQuery");
			
			try{
				switch (idQuery){
			
					case "1" : {
						String idLotto = null;
						String idPart = null;
						idLotto = request.getParameter("idLotto");
						idPart = request.getParameter("idPart");
						conn = DbUtils.getMySqlConnection();
						rs = DbUtils.getLotto(conn, idLotto, idPart);
						createXML(rs, out);
						break;
					}
					
					case "2" : {
						String idGioc= null;
						String idPart= null;
						idGioc = request.getParameter("idGioc");
						idPart = request.getParameter("idPart");
						conn = DbUtils.getMySqlConnection();
						rs = DbUtils.getPos(conn, idGioc, idPart);
						createXML(rs, out);
						break;
					}
					
					case "3" : {
						String idGioc= null;
						String idPart= null;
						String pos = null;
						idGioc = request.getParameter("idGioc");
						idPart = request.getParameter("idPart");
						pos = request.getParameter("pos");
						conn = DbUtils.getMySqlConnection();
						DbUtils.setPos(conn, idGioc, idPart, pos);
						break;
						
					}
					
					case "4" : {
						conn = DbUtils.getMySqlConnection();
						rs =DbUtils.getPartite(conn);
						createXML(rs, out);
						break;
					}
					
					case "5" :{
						String nGioc = null;
						String icona = null;
						nGioc = request.getParameter("nomeGioc");
						icona = request.getParameter("icona");
						conn = DbUtils.getMySqlConnection();
						DbUtils.setCreatorePartita(conn, nGioc, icona);
						break;
					}
					
					case "6" : {
						String nGioc = null;
						String icona = null;
						String idPart = null;
						nGioc = request.getParameter("nomeGioc");
						icona = request.getParameter("icona");
						idPart = request.getParameter("idPart");
						conn = DbUtils.getMySqlConnection();
						DbUtils.setGiocatore(conn, nGioc, icona, idPart);
						break;
					}
					
					case "7" : {
						String idPart = null;
						String idGioc = null;
						idGioc = request.getParameter("idGioc");
						idPart = request.getParameter("idPart");
						conn = DbUtils.getMySqlConnection();
						DbUtils.setTurno(conn, idGioc, idPart);
						break;
					}
					
					case "8" : {
						String idPart = null;
						String idGioc = null;
						String soldi = null;
						idGioc = request.getParameter("idGioc");
						idPart = request.getParameter("idPart");
						soldi = request.getParameter("soldi");
						conn = DbUtils.getMySqlConnection();
						DbUtils.setSoldi(conn, soldi, idGioc, idPart);
						break;
					}
					
					case "9" : {
						String idPart = null;
						String idGioc = null;
						idGioc = request.getParameter("idGioc");
						idPart = request.getParameter("idPart");
						conn = DbUtils.getMySqlConnection();
						rs = DbUtils.getSoldi(conn, idGioc, idPart);
						createXML(rs, out);
						break;
					}
					
					case "10" : {
						String idPart = null;
						String idGioc = null;
						String nCarta = null;
						idGioc = request.getParameter("idGioc");
						idPart = request.getParameter("idPart");
						nCarta = request.getParameter("nCarta");
						conn = DbUtils.getMySqlConnection();
						DbUtils.setNCarte(conn, idGioc, idPart, nCarta);
						break;
					}
					
					case "11" : {
						String idPart = null;
						String idGioc = null;
						idGioc = request.getParameter("idGioc");
						idPart = request.getParameter("idPart");
						conn = DbUtils.getMySqlConnection();
						rs = DbUtils.getNCarte(conn, idPart, idGioc);
						createXML(rs, out);
						break;
					}
					
					case "12" : {
						String idImpr = null;
						idImpr = request.getParameter("IdImpr");
						conn = DbUtils.getMySqlConnection();
						rs = DbUtils.getImprevisto(conn, idImpr);
						createXML(rs, out);
						break;
					}
					
					case "13" : {
						String idProb = null;
						idProb = request.getParameter("IdProb");
						conn = DbUtils.getMySqlConnection();
						rs = DbUtils.getProbabilita(conn, idProb);
						createXML(rs, out);
						break;
					}
					
					case "14" : {
						String idProp;
						String nomePart;
						String nMaxGioc;
						String nGiocCorr;
						idProp = request.getParameter("idProp");
						nomePart = request.getParameter("nomePart");
						nMaxGioc = request.getParameter("nMaxGioc");
						nGiocCorr = request.getParameter("nGiocCorr");
						conn = DbUtils.getMySqlConnection();
						DbUtils.createPartita(conn, idProp, nomePart, nMaxGioc, nGiocCorr);
						break;
					}
					
					case "15" : {
						String idPart = null;
						idPart = request.getParameter("idPart");
						conn = DbUtils.getMySqlConnection();
						rs = DbUtils.getNGiocCorr(conn, idPart);
						createXML(rs, out);
						break;
					}
					
					case "16" : {
						String idPart = null;
						idPart = request.getParameter("idPart");
						conn = DbUtils.getMySqlConnection();
						DbUtils.setIniziata(conn, idPart);
						break;
					}
					
					case "17" : {
						String idPart = null;
						String idLotto = null;
						idPart = request.getParameter("idPart");
						idLotto = request.getParameter("idLotto");
						conn = DbUtils.getMySqlConnection();
						rs = DbUtils.getNCase(conn, idPart, idLotto);
						createXML(rs, out);
						break;
					}
					
					case "18" : {
						String idPart = null;
						String idLotto = null;
						idPart = request.getParameter("idPart");
						idLotto = request.getParameter("idLotto");
						conn = DbUtils.getMySqlConnection();
						rs = DbUtils.getNAlberghi(conn, idPart, idLotto);
						createXML(rs, out);
						break;
					}
					
					case "19" :{
						String idPart = null;
						String idGioc = null;
						idGioc = request.getParameter("idGioc");
						idPart = request.getParameter("idPart");
						conn = DbUtils.getMySqlConnection();
						rs = DbUtils.getInfoGiocatore(conn, idPart, idGioc);
						createXML(rs, out);
						break;
					}	
					
				}
			
				
				
			}
		
		catch (ClassNotFoundException e) {
			out.print("error class");
			e.printStackTrace();
		} catch (SQLException e) {
			out.print("error sql");
			e.printStackTrace();
		}finally{
			if (conn != null) {
				try {
					conn.close();
				}
				catch (SQLException ignored){}
				}
		}
	}
}

	
	
	public void createXML(ResultSet rs, PrintWriter out) throws SQLException{
		
		ResultSetMetaData meta =  rs.getMetaData(); //valori medadata
		int numCol= meta.getColumnCount();
		out.append("<tabella>");
		for(int i = 1; i < numCol+1; i++){
			out.append("<colIntestazione>"+meta.getColumnName(i)+"</colIntestazione>");   //non � getColumnLabel??
		}
		while(rs.next()){
			out.append("<riga>");
			for(int i = 1; i < numCol+1; i++){
				out.append("<col>"+rs.getString(meta.getColumnName(i))+"</col>");
			}
			out.append("</riga>");	
		}
		
		out.append("</tabella>");
	}
	
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
