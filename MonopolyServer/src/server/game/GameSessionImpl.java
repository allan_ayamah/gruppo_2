package server.game;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collection;

import server.Turn;
import server.action.ActionBuySell;
import server.action.DrawCard;
import server.board.BoardConfigImpl;
import server.board.DefaultBoardBuilder;
import server.notif.EventBroadcastStrategy;
import server.notif.GameStartBroadcast;
import shared.Message;
import shared.TurnState;
import shared.action.TurnAction;
import shared.bean.PlayerInfoBean;
import shared.bean.SessionBean;
import shared.board.BoardConfig;
import shared.board.CardType;
import shared.board.PropertyColorGroup;
import shared.board.PropertySpace;
import shared.board.Space;
import shared.board.SpaceType;
import shared.exception.GameException;
import shared.exception.SessionFullException;
import shared.notification.BroadcastStrategy;
import shared.notification.EventType;
import shared.notification.GameEvent;
import shared.player.SessionPlayer;
import shared.service.GameSession;

public class GameSessionImpl extends UnicastRemoteObject implements GameSession {

	private static final long serialVersionUID = -8695074007565065187L;

	public static enum SessionSate {
		BEFORE_GAME, IN_PROGRES, ENDED;
	}

	private String id;
	private ArrayList<SessionPlayer> players;
	private BoardConfigImpl board;

	private SessionPlayer currentPlayer;
	private int indexOfCurrentPlayer;

	private GameSessionImpl gameSession;
	private Turn turn = null;

	public SessionSate state;
	private Collection<PropertyColorGroup> availableTokens = new ArrayList<PropertyColorGroup>(MAX_PLAYERS);

	public GameSessionImpl() throws RemoteException {
		super();
		players = new ArrayList<SessionPlayer>(MAX_PLAYERS);
		board = new BoardConfigImpl(new DefaultBoardBuilder());
		state = SessionSate.BEFORE_GAME;

		System.out.println("Game session created");

	}

	public GameSessionImpl(SessionPlayer player) throws RemoteException {
		this();
		gameSession = this;

	}

	public SessionBean addPlayer(SessionPlayer client) throws SessionFullException, RemoteException {
		synchronized (players) {

			if (sessionFull())
				throw new SessionFullException();
			else {
				try {
					client.setId(getPlayers().size());
					initPlayer(client);
					players.add(client);

					SessionBean sessionDetails = new SessionBean(this.id, client.getId(), (GameSession) this,
							getBoardConfiguration());

					return sessionDetails;
				} catch (RemoteException e) {
					return null;

				}
			}
		}
	}

	private void initPlayer(SessionPlayer player) throws RemoteException {

		int casual = 0;
		int types = 7;
		int playerCash = 1500; //
		while (types > 0) {
			casual = (int) Math.floor(Math.random() * 7) + 1;
			playerCash += cashSetter(casual);
			types--;
		}
		player.initCash(playerCash);

		//System.out.println(player.getName() + ": initial cash is " + playerCash);

		// Initial player contracts

		ArrayList<Integer> contracts = new ArrayList<Integer>();
		while (contracts.size() < 5) {
			int index = (int) Math.floor(Math.random() * ((BoardConfig.MAX_SPACES - 1) - 1)) + 1;
			PropertySpace prop = getSpace(index).getProperty();
			if (prop != null) {
				if (prop.getDeed() != null) {
					if (prop.getDeed().getOwner() == null) {
						prop.getDeed().setOwner(player);
						contracts.add(index);
						// System.out.println(prop.getDeed().getOwner());
					}

				}

			}

		}

		//System.out.println(player.getName() + ": initial properties count " + contracts.size());

		player.setProperties(contracts);
	}

	public void broadcast(BroadcastStrategy strategy) throws RemoteException {
		ArrayList<SessionPlayer> dead = new ArrayList<SessionPlayer>();
		synchronized (players) {
			for (SessionPlayer player : getPlayers()) {
				try {
					strategy.doBroadcast(gameSession, player);
				} catch (RemoteException e) {
					dead.add(player);
					e.printStackTrace();
				}
			}
			for (SessionPlayer player : dead)
				players.remove(player);
		}

	}

	public void startGame() throws GameException, RemoteException {
		System.out.println("starting game");
		if (players.size() < MIN_PLAYERS)
			throw new GameException("YOU CANNOT PLAY ALONE. FIND A FRIEND, GET A LIFE!");

		state = SessionSate.IN_PROGRES;
		indexOfCurrentPlayer = (int) Math.floor(Math.random() * players.size());
		currentPlayer = getPlayer(indexOfCurrentPlayer);

		broadcast(new GameStartBroadcast(this));
		takeTurn(currentPlayer);
	}

	public void endGame() {

	}

	public void endTurn() {
		if (indexOfCurrentPlayer == (players.size() - 1))
			indexOfCurrentPlayer = 0;
		else
			indexOfCurrentPlayer++;
		try {
			getPlayer(indexOfCurrentPlayer).notifyGameUpdate(new GameEvent(EventType.TAKE_TURN));
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		takeTurn(getPlayer(indexOfCurrentPlayer));
	}

	public void takeTurn(SessionPlayer player) {
		currentPlayer = player;
		try {
			System.out.println("selected player" + currentPlayer.getName());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		turn = new Turn(currentPlayer, gameSession);
		try {
			broadcast(new EventBroadcastStrategy(
					turn.prepareEventWithMessage(EventType.TAKE_TURN, player.getName() + "'S TURN")));
		} catch (RemoteException e) {
			System.out.println("Take turn, remote error");
		}
	}

	private boolean sessionFull() {
		return players.size() == MAX_PLAYERS;
	}

	public ArrayList<SessionPlayer> getPlayers() {
		return players;
	}

	@Override
	public void rollDice(int playerId) throws RemoteException {
		if (validateCurrPlayerId(playerId)) {
			turn.roll();
		} else
			getPlayer(playerId).notifyMessage(new Message("IT'S NOT YOUR TURN, YET."));
	}

	@Override
	public void drawCard(int playerId) throws RemoteException, GameException {
		if (validateCurrPlayerId(playerId) && !turn.state.equals(TurnState.PRE_MOVE)) {
			
			System.out.println("Draw card");
			Space space = getSpace(currentPlayer.getCurrentSpace());
			if (space.getType().equals(SpaceType.CHANCE))
				turn.actionExecuter(new DrawCard(CardType.CHANCE));
			else
				turn.actionExecuter(new DrawCard(CardType.COMUNITYCHEST));
		}
	}

	@Override
	public void buyProperty(int playerId) throws RemoteException, GameException {
		System.out.println("Buy property");
		if (validateCurrPlayerId(playerId) && !turn.state.equals(TurnState.PRE_MOVE)) {
			Space space = getSpace(currentPlayer.getCurrentSpace());
			turn.actionExecuter(new ActionBuySell(space, TurnAction.BUY));
		}

	}

	@Override
	public void sellProperty(int playerId) throws RemoteException, GameException {
		if (validateCurrPlayerId(playerId) && !turn.state.equals(TurnState.PRE_MOVE)) {
			Space space = getSpace(currentPlayer.getCurrentSpace());
			turn.actionExecuter(new ActionBuySell(space, TurnAction.SELL));
		}
	}

	public Space getSpace(int ind) throws RemoteException {
		return board.getSpaces().get(ind);
	}

	public SessionPlayer getPlayer(int index) {
		return getPlayers().get(index);
	}

	public SessionPlayer getCurrentplayer() throws RemoteException {
		return currentPlayer;
	}

	protected boolean validateCurrPlayerId(int playerId) throws RemoteException {
		return playerId == currentPlayer.getId();
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() throws RemoteException {
		return id;
	}

	public BoardConfig getBoardConfiguration() {
		return board;
	}

	@Override
	public ArrayList<PlayerInfoBean> getPlayersInfo() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSessionId() throws RemoteException {
		return getId();
	}

	@Override
	public int getCurrentPlayerId() throws RemoteException {
		// TODO Auto-generated method stub
		return currentPlayer.getId();
	}

	@Override
	public Collection<PropertyColorGroup> getAvailableTokens() throws RemoteException {

		return availableTokens;
	}

	@Override
	public void endTurn(int id) throws RemoteException {
		if (validateCurrPlayerId(id) && !turn.state.equals(TurnState.PRE_MOVE))
			turn.end();
	}

	@Override
	public TurnState getTurnState() throws RemoteException {
		return turn.state;

	}

	private int cashSetter(int i) {
		int cash = 0;
		switch (i) {
		case 1:
			cash = 500;
			break;
		case 2:
			cash = 100;
			break;
		case 3:
			cash = 50;
			break;
		case 4:
			cash = 20;
			break;
		case 5:
			cash = 10;
			break;
		case 6:
			cash = 5;
			break;
		case 7:
			cash = 1;
			break;
		}

		return cash;
	}
}
