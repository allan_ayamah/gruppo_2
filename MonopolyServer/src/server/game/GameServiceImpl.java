package server.game;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

import shared.bean.SessionBean;
import shared.exception.SessionFullException;
import shared.player.SessionPlayer;
import shared.service.GameService;
import shared.service.GameSession;

public class GameServiceImpl extends UnicastRemoteObject implements GameService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Map<String, GameSessionImpl> sessions;

	public GameServiceImpl() throws RemoteException {
		super();
		sessions = new HashMap<String, GameSessionImpl>();
	}

	@Override
	public SessionBean createSession(SessionPlayer player) throws RemoteException {
		synchronized (sessions) {
			System.out.println("Creating session");
			GameSessionImpl gameSession = new GameSessionImpl(player);
			String sessionId = generateNewSessionId();

			// Salva la nuova sessione
			sessions.put(sessionId, gameSession);
			System.out.println("Session created");
			return joinSession(sessionId, player);
		}
	}

	@Override
	public SessionBean joinSession(String sessionId, SessionPlayer player) throws RemoteException {
		synchronized (sessions) {
			if (isSessionSet(sessionId)) {
				try {
					SessionBean sDetails = getSession(sessionId).addPlayer(player);
					System.out.println("GameService->joinSession()");
					return sDetails;
				} catch (SessionFullException e) {
					e.getMessage();
				}
			}
		}
		return null;
	}

	@Override
	public void closeSession(GameSession session) throws RemoteException {
		synchronized (sessions) {

		}

	}

	private GameSessionImpl getSession(String sessionId) {
		return (GameSessionImpl) sessions.get(sessionId);
	}

	private final boolean isSessionSet(SessionBean session) {
		return isSessionSet(session.getSessionId());
	}

	private final boolean isSessionSet(String key) {
		return sessions.containsKey(key);
	}

	private static final String generateNewSessionId() {
		String id = "session_";
		id += Integer.toString(sessions.size());
		return id;
	}

	public Map<String, GameSessionImpl> getSessions() {
		return sessions;
	}

	@Override
	public String getSome(String name) throws RemoteException {
		return name;
	}

}
