package server.board;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;

import shared.board.Card;
import shared.board.Space;

public interface BoardConfigBuilder extends Remote{
	public Collection<Space> buildSpaces() throws RemoteException;
	public Collection<Card> buildChanceCards() throws RemoteException;
	public Collection<Card> buildCommunityChestCards() throws RemoteException;
}
