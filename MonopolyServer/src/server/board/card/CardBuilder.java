package server.board.card;

import server.action.ActionChangeStatus;
import server.action.ActionGetCash;
import server.action.ActionGoto;
import server.action.ActionPay;
import shared.board.Card;
import shared.player.AbstractSessionPlayer.PlayerStatus;

public class CardBuilder {

	public Card buildChance(String[] cardProperties) {
		Card card = new Card();

		card.setText(cardProperties[0]);

		if (cardProperties[1] != null && !cardProperties[1].equals("")) {
			card.setStrategy(new ActionGetCash(Integer.parseInt(cardProperties[1])));
		} else if (cardProperties[2] != null && !cardProperties[2].equals("")) {
			// System.out.println(cardProperties[2]);
			card.setStrategy(new ActionPay(Integer.parseInt(cardProperties[2])));
		} else if (cardProperties[3] != null && !cardProperties[3].equals(""))
			card.setStrategy(new ActionGoto(Integer.parseInt(cardProperties[3])));
		else if (cardProperties[4] != null && !cardProperties[4].equals("")) {
			//System.out.println("jail free");
			card.setStrategy(new ActionChangeStatus(PlayerStatus.FREE));
		}

		return card;
	}

	public Card buildCommunityChest(String[] cardProperties) {
		Card card = new Card();

		card.setText(cardProperties[0]);

		int size = cardProperties.length;
		if (size <= 3) {
			if (cardProperties[1] != null && !cardProperties[1].equals("")) {
				//System.out.println(card.getText());
				card.setStrategy(new ActionGetCash(Integer.parseInt(cardProperties[1])));
			} else if (cardProperties[2] != null && !cardProperties[2].equals("")) {
				card.setStrategy(new ActionPay(Integer.parseInt(cardProperties[2])));
			}
		} else {
			if (cardProperties[3] != null && cardProperties[1] != null && !cardProperties[3].equals("")
					&& !cardProperties[1].equals("")) {
				// Goto space if pass go apply getcash strategy
				//System.out.println("go and get cash");
				card.setStrategy(new ActionGoto(Integer.parseInt(cardProperties[3]),
						new ActionGetCash(Integer.parseInt(cardProperties[1]))));
			} else {
				if (cardProperties[3] != null && !cardProperties[3].equals("")) {
					//System.out.println(card.getText() + " 3");
					card.setStrategy(new ActionGoto(Integer.parseInt(cardProperties[3]), true));
				}
			}
			if(size > 4)
			if (cardProperties[4] != null && !cardProperties[4].equals("")) {
				//System.out.println(cardProperties[0]);
				card.setStrategy(new ActionKeepCard());
			}
		}
		return card;
	}

}
