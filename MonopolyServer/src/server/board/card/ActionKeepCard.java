package server.board.card;

import java.rmi.RemoteException;

import shared.action.ActionStrategy;
import shared.service.GameSession;

public class ActionKeepCard implements ActionStrategy {

	@Override
	public void applyEffect(GameSession session, int playerId) throws RemoteException {
		session.getPlayer(playerId).setOutOfJailCard(true);

	}

}
