package server.board.space;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import server.action.Action;
import shared.action.ActionStrategy;
import shared.board.PropertyColorGroup;
import shared.board.PropertySpace;
import shared.board.Space;
import shared.board.SpaceType;
import shared.player.SessionPlayer;

public class SpaceImpl extends UnicastRemoteObject implements Space {

	private static final long serialVersionUID = 624662845737301642L;

	protected String name;
	protected SpaceType type;
	protected Action effect;
	
	protected ActionStrategy actionStrategy;
	
	
	public ActionStrategy getActionStrategy() {
		return actionStrategy;
	}

	public void setActionStrategy(ActionStrategy actionStrategy) {
		this.actionStrategy = actionStrategy;
	}

	protected boolean canBuildOn = false;
	protected PropertySpace property;
	protected PropertyColorGroup group = null;

	public SpaceImpl(SpaceType type) throws RemoteException {
		this.type = type;
		setCanBuildOn(false);
	}

	public void setProperty(PropertySpace prop) {
		this.property = prop;
	}

	public void setPropertyGroup(PropertyColorGroup g) {
		this.group = g;
	}

	@Override
	public void setCanBuildOn(boolean canBuildOn) {
		this.canBuildOn = canBuildOn;
	}

	public void setEffect(Action effect) {
		this.effect = effect;
	}

	@Override
	public SpaceType getType() {
		return type;
	}

	@Override
	public boolean canBuildOn() {
		return canBuildOn;
	}


	@Override
	public PropertySpace getProperty() {
		return property;
	}

	@Override
	public boolean isOwned() {
		boolean owned = false;
		if(getProperty() != null){
			owned = getProperty().getDeed().getOwner() != null;
		}
	
		return owned;
	}

	@Override
	public void setOwner(SessionPlayer owner) {
		// getDeed().setOwner(owner);
	}

	@Override
	public PropertyColorGroup getGroup() {
		return group;
	}

	@Override
	public String getName() throws RemoteException {
		return name;
	}
	
	public void setName(String n){
		this.name = n;
	}

	@Override
	public int getOwner() {
		// TODO Auto-generated method stub
		return 0;
	}


}
