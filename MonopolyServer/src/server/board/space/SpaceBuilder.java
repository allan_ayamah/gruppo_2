package server.board.space;

import java.io.Serializable;
import java.rmi.RemoteException;

import server.action.Action;
import server.action.ActionGoto;
import shared.Deed;
import shared.board.BoardConfig;
import shared.board.PropertyColorGroup;
import shared.board.PropertySpace;
import shared.board.Space;
import shared.board.SpaceType;

public class SpaceBuilder implements Serializable {

	public Space buildGo(String name) {
		return buildCornerSpace(SpaceType.GO, name);
	}

	public Space buildJail(String name) {
		return buildCornerSpace(SpaceType.JAIL, name);
	}

	public Space buildGotoJail(String name) {
		return buildCornerSpace(SpaceType.GOTOJAIL, name);
	}

	public Space buildParking(String name) {
		return buildCornerSpace(SpaceType.PARKING, name);
	}

	public Space buildIncomeTax(String name) {
		return buildSpace(SpaceType.INCOMETAX, name);
	}

	public Space buildLuxuryTax(String name) {
		return buildSpace(SpaceType.LUXURYTAX, name);
	}

	// Porperty
	public Space buildUtilty(String name, Deed contract) {
		return buildPropertySpace(SpaceType.UTILITY, name, contract);
	}

	public Space buildRailwayStation(String name, Deed contract) {
		return buildPropertySpace(SpaceType.RAILWAYSTATION, name, contract);
	}

	public Space buildStreet(String name, Deed d, PropertyColorGroup group) {
		SpaceImpl space = (SpaceImpl) buildPropertySpace(SpaceType.STREET, name, d);
		space.setPropertyGroup(group);
		space.setName(name);
		space.getProperty().setHotelNumber(0);
		space.getProperty().setHouseNumber(0);
		return (Space) space;
	}

	public Space buildChance(String name) {
		return buildSpace(SpaceType.CHANCE, name);
	}

	public Space buildCommunityChect(String name) {
		return buildSpace(SpaceType.COMMUNITYCHEST, name);
	}

	// Methodi privati per la construzioni dei vari lotti
	private Space buildCornerSpace(SpaceType type,String name){
		SpaceImpl space = buildSpace(type,name);
		space.setCanBuildOn(false);
		
		if(type.equals(SpaceType.GOTOJAIL)){
			space.setEffect(new Action(new ActionGoto(BoardConfig.GO)));
		}
		
		
		return space;
	}

	public SpaceImpl buildSpace(SpaceType type, String name) {
		SpaceImpl space = null;
		try {
			space = new SpaceImpl(type);
			space.setName(name);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return space;
	}

	private SpaceImpl buildPropertySpace(SpaceType type, String name, Deed contract) {
		SpaceImpl space = buildSpace(type, name);
		PropertySpace property = new PropertySpace(contract);
		//System.out.println(property.getDeed());
		space.setProperty(property);
		return space;
	}

}
