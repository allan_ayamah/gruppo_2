package server.board;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import server.board.card.CardBuilder;
import shared.board.BoardConfig;
import shared.board.Card;
import shared.board.Space;

public class BoardConfigImpl extends UnicastRemoteObject implements BoardConfig {
	/**
	 * 
	 */
	private static final long serialVersionUID = -833839519590673252L;



	private BoardConfigBuilder boardEngineer;
	private CardBuilder cardEngineer;

	private ArrayList<Space> spaces;
	private ArrayList<Card> communityChestCards;
	private ArrayList<Card> chanceCards;

	public BoardConfigImpl() throws RemoteException {
		// TODO Auto-generated constructor stub
	}	
	public BoardConfigImpl(final BoardConfigBuilder boardBuilder) throws RemoteException {
		boardEngineer = boardBuilder;
		spaces = (ArrayList<Space>) boardBuilder.buildSpaces();
		chanceCards = (ArrayList<Card>) boardEngineer.buildChanceCards();
		communityChestCards = (ArrayList<Card>) boardEngineer.buildCommunityChestCards();
	}

	@Override
	public ArrayList<Space> getSpaces() {
		return spaces;
	}

	@Override

	public ArrayList<Card> getCommunityChestCards(){
		return communityChestCards;
	}

	@Override

	public ArrayList<Card> getChanceCards(){
		return chanceCards;
	}

}
