package server.board;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;

import server.board.card.CardBuilder;
import server.board.space.SpaceBuilder;
import shared.Deed;
import shared.board.Card;
import shared.board.PropertyColorGroup;
import shared.board.Space;
import shared.board.SpaceType;

public class DefaultBoardBuilder implements BoardConfigBuilder {
	private static final String SPACES_FILE = "serverAssets/spaces.csv";
	private static final String COMMUNITY_CARDS_FILE = "serverAssets/community.csv";
	private static final String CHANCE_CARDS_FILE = "serverAssets/chance.csv";
	
	private static final String COL_DIVIDER = ",";
	
	private BufferedReader br;
	private static String line = "";
	
	
	public DefaultBoardBuilder() {
		BufferedReader br = null;
	}
	
	
	
	@Override
	public Collection<Space> buildSpaces() {
		Collection <Space> spaces = new ArrayList<Space>(BoardConfigImpl.MAX_SPACES);
		
		SpaceBuilder builder = new SpaceBuilder();
		try {
			URL fileUri = getClass().getResource(SPACES_FILE);
			br = new BufferedReader(new InputStreamReader(new FileInputStream(SPACES_FILE), StandardCharsets.UTF_8));
			while ((line = br.readLine()) != null) {
				String[] property = line.split(COL_DIVIDER);
				String spaceType = property[2];
				String name = property[0];
				
				Deed propertyContract;
				switch (spaceType) {
				case "STREET":
					 propertyContract = new Deed();
					 propertyContract.setPurchasePrice(Integer.parseInt(property[4]));
					 
					 propertyContract.setRentPrice(Integer.parseInt(property[5]));
					 propertyContract.setRentPrice(Integer.parseInt(property[6]));
					 propertyContract.setRentPrice(Integer.parseInt(property[7]));
					 propertyContract.setRentPrice(Integer.parseInt(property[8]));
					 propertyContract.setRentPrice(Integer.parseInt(property[9]));
					 
					 propertyContract.setHouseBuildingCost(Integer.parseInt(property[11]));
					 propertyContract.setHotelBuildingCost(Integer.parseInt(property[12]));
					
					 spaces.add(builder.buildStreet(name,propertyContract, PropertyColorGroup.valueOf(property[3])));
					break;		
				case "COMMUNITYCHEST":
					 spaces.add(builder.buildCommunityChect(name));
					break;	
				case "CHANCE":
					 spaces.add(builder.buildChance(name));
					break;
				case "RAILWAYSTATION":
					 propertyContract = new Deed();
					 if(!property[4].equals("")) // verifica
					 propertyContract.setPurchasePrice(Integer.parseInt(property[4]));
					 
					 propertyContract.setRentPrice(Integer.parseInt(property[5]));
					 propertyContract.setRentPrice(Integer.parseInt(property[6]));
					 propertyContract.setRentPrice(Integer.parseInt(property[7]));
					 propertyContract.setRentPrice(Integer.parseInt(property[8]));
					
					 spaces.add(builder.buildRailwayStation(name,propertyContract));
					break;	
				case "UTILITY":
					 spaces.add(builder.buildUtilty(name,null));
					break;
				default:
					spaces.add(builder.buildSpace(SpaceType.valueOf(spaceType),name));
				break;	
			}
				
			}
	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return spaces;
	}

	@Override
	public Collection<Card> buildChanceCards() {
		ArrayList<Card> cards = new ArrayList<Card>();
		CardBuilder builder = new CardBuilder();
		
		try {
			URL fileUri = getClass().getResource(CHANCE_CARDS_FILE);
			br = new BufferedReader(new InputStreamReader(new FileInputStream(CHANCE_CARDS_FILE), StandardCharsets.UTF_8));
			while ((line = br.readLine()) != null) {
				line =line.trim();
				String[] cardProperties = line.split(COL_DIVIDER);				
				cards.add(builder.buildChance(cardProperties));
				
			}
				
	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return cards;
		
	}

	@Override
	public Collection<Card> buildCommunityChestCards() {
		
		System.out.println("build com");
		ArrayList<Card> cards = new ArrayList<Card>();
		CardBuilder builder = new CardBuilder();
		
		try {
			URL fileUri = getClass().getResource(COMMUNITY_CARDS_FILE);
			br = new BufferedReader(new InputStreamReader(new FileInputStream(COMMUNITY_CARDS_FILE), StandardCharsets.UTF_8));
			while ((line = br.readLine()) != null) {
				line =line.trim();
				String[] cardProperties = line.split(COL_DIVIDER);				
				cards.add(builder.buildCommunityChest(cardProperties));
				
			}
				
	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return cards;
	}

}
