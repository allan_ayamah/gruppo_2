package server.action;

import java.io.Serializable;
import java.rmi.RemoteException;

import server.game.GameSessionImpl;
import shared.action.ActionStrategy;
import shared.service.GameSession;

public class Action implements Serializable {
	
	
	private ActionStrategy strategy;
	
	public Action(ActionStrategy strategy) {
		 setStrategy(strategy);
	}
	
	public Action(){
		
	}
	
	public void setStrategy(ActionStrategy strategy) {
		this.strategy = strategy;
	}
	
	
	public void applyAction(GameSession session,int playerId){
		try {
			strategy.applyEffect(session,playerId);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}