package server.action;

import java.rmi.RemoteException;

import shared.action.ActionStrategy;
import shared.player.SessionPlayer;
import shared.service.GameSession;

public class ActionPay implements ActionStrategy {
	private int amount;
	private int recieverId = -1;
	
	public ActionPay(int amt) {
		this.amount = amt;
	}
	
	public ActionPay(int amt, int dest){
		this(amt);
		this.recieverId = dest;
	}
	

	@Override
	public void applyEffect(GameSession session, int senderId) throws RemoteException {
		SessionPlayer sender = session.getPlayer(senderId);
		if(recieverId >= 0){
			session.getPlayer(recieverId).addCash(amount);
		}
		sender.subCash(amount);
	}
	
}
