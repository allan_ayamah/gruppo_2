package server.action;

import java.rmi.RemoteException;

import server.ServerHelper;
import shared.Message;
import shared.action.ActionStrategy;
import shared.action.TurnAction;
import shared.board.Space;
import shared.notification.EventType;
import shared.notification.GameEvent;
import shared.player.SessionPlayer;
import shared.service.GameSession;

public class ActionBuySell implements ActionStrategy {

	private TurnAction action;
	private Space saidSpace;

	public ActionBuySell(Space space, TurnAction action) {
		saidSpace = space;
		this.action = action;
	}

	@Override
	public void applyEffect(GameSession session, int playerId) throws RemoteException {

		if (TurnAction.BUY.equals(action)) {
			if (saidSpace.isOwned()) {
				System.out.println("ActionBuySell.applyEffect()-> owned property");
				GameEvent ev = new GameEvent(EventType.SELL);
				session.getPlayer(saidSpace.getOwner()).notifyGameUpdate(ev);
			}
			else{
				
				SessionPlayer buyer = session.getPlayer(playerId);
				int propertyprice = saidSpace.getProperty().getDeed().getPurchasePrice();

				if(buyer.getCurrentCash() >= propertyprice){
					Action action = new Action(new ActionPay(propertyprice));
					action.applyAction(session, playerId);
					saidSpace.getProperty().getDeed().setOwner(session.getPlayer(playerId));
				}else{
					buyer.notifyMessage(new Message("YOU DON'T HAVE ENOUGH MONEY TO BUY THIS PROPERTY."));
				}
				
			}

		} else if (TurnAction.SELL.equals(action)) {
			
			//Permission granted to sell property
			SessionPlayer buyer = session.getPlayer(playerId);
			SessionPlayer seller = session.getPlayer(saidSpace.getOwner());
			int propertyprice = saidSpace.getProperty().getDeed().getPurchasePrice();

			if(buyer.getCurrentCash() >= propertyprice){
				Action action = new Action(new ActionPay(propertyprice,seller.getId()));
				action.applyAction(session, playerId);
				saidSpace.getProperty().getDeed().setOwner(session.getPlayer(playerId));
			}else{
				buyer.notifyMessage(new Message("YOU DON'T HAVE ENOUGH MONEY TO BUY THIS PROPERTY."));
			}

		}
	}

}
