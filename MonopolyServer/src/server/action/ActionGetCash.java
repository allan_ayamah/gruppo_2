package server.action;

import java.rmi.RemoteException;

import shared.action.ActionStrategy;
import shared.service.GameSession;

public class ActionGetCash implements ActionStrategy {
	
	private int amount;
	
	public ActionGetCash(int amt) {
		this.amount = amt;
	}
		
	@Override
	public void applyEffect(GameSession session, int playerId) throws RemoteException {
		
		session.getPlayer(playerId).addCash(amount);;

	}

}
