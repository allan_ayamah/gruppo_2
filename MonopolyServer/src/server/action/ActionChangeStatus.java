package server.action;

import java.rmi.RemoteException;

import shared.action.ActionStrategy;
import shared.player.AbstractSessionPlayer.PlayerStatus;
import shared.service.GameSession;

public class ActionChangeStatus implements ActionStrategy{
	
	private PlayerStatus status;
	public ActionChangeStatus(PlayerStatus stat) {
		status = stat;
	}

	@Override
	public void applyEffect(GameSession session, int playerId) throws RemoteException {
		session.getPlayer(playerId).setStatus(status);
		
	}

}
