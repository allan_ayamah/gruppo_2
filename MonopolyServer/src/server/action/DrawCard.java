package server.action;

import java.rmi.RemoteException;

import shared.action.ActionStrategy;
import shared.board.Card;
import shared.board.CardType;
import shared.service.GameSession;

public class DrawCard implements ActionStrategy {

	private CardType cardType;

	public DrawCard(CardType type) {
		cardType = type;
	}

	@Override
	public void applyEffect(GameSession session, int playerId) {
		Card card;
		int lastCardInd;
		if (cardType.equals(CardType.CHANCE)) {

			try {
				lastCardInd = session.getBoardConfiguration().getChanceCards().size() - 1;
				card = session.getBoardConfiguration().getChanceCards().get(lastCardInd);
				//card.applyEffect(session, session.getPlayer(playerId));
			} catch (RemoteException e) {
				System.out.println("Remote error -> DrawCard.applyEffect()");
				e.printStackTrace();
			}

		}
		else {
			try {
				lastCardInd = session.getBoardConfiguration().getCommunityChestCards().size() - 1;
				card = session.getBoardConfiguration().getCommunityChestCards().get(lastCardInd);
				//card.applyEffect(session, session.getPlayer(playerId));
			} catch (RemoteException e) {
				System.out.println("Remote error -> DrawCard.applyEffect()");
			}

		}

	}

}
