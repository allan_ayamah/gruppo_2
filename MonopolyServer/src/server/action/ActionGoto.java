package server.action;

import java.rmi.RemoteException;

import server.notif.EventBroadcastStrategy;
import shared.action.ActionStrategy;
import shared.bean.PlayerInfoBean;
import shared.board.BoardConfig;
import shared.notification.EventType;
import shared.notification.GameEvent;
import shared.player.SessionPlayer;
import shared.service.GameSession;

public class ActionGoto implements ActionStrategy{
	
	private int space;
	private boolean takeSteps = false;
	private ActionStrategy getCashAction;
	
	public ActionGoto(int space) {
		this.space = space;
	}
	
	public ActionGoto(int stepsCount, boolean tSteps) {
		this(stepsCount);
		this.takeSteps =tSteps;
	}
	
	public ActionGoto(int space, ActionStrategy strat) {
		this(space,true);
		getCashAction = strat;
	}
	
	

	@Override
	public void applyEffect(GameSession session, int playerId) throws RemoteException {
		SessionPlayer player = session.getPlayer(playerId);
	
		if(takeSteps){
			space = session.getPlayer(playerId).getCurrentSpace() + space;
			if(space > BoardConfig.MAX_SPACES - 1){
				getCashAction.applyEffect(session, playerId);
			}
		}

		GameEvent ev = new GameEvent(EventType.GOTO);
		ev.setSender(new PlayerInfoBean(playerId, session.getPlayer(playerId).getName()));
		ev.setMoveVal(space);
		
		session.broadcast(new EventBroadcastStrategy(ev));
	}

}
