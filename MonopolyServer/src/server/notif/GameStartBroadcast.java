package server.notif;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;

import server.game.GameSessionImpl;
import shared.Message;
import shared.bean.PlayerInfoBean;
import shared.bean.SessionBean;
import shared.notification.BroadcastStrategy;
import shared.notification.EventType;
import shared.notification.GameEvent;
import shared.player.SessionPlayer;
import shared.service.GameSession;

public class GameStartBroadcast implements BroadcastStrategy,Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4205020808836995709L;
	private GameSessionImpl session;
	private ArrayList<PlayerInfoBean> playersInfo;
	
	public GameStartBroadcast( GameSessionImpl session) {
		this.session = session;
		playersInfo = getPlayersInfo();
		
	}

	@Override
	public void doBroadcast(GameSession s, SessionPlayer currP) {

		SessionBean bean;
		try {
			bean = new SessionBean(session.getId(), currP.getId(), session, session.getBoardConfiguration());
			bean.setPlayersInfo(playersInfo);
			
			currP.initPlayground(bean);
			
			GameEvent ev = new GameEvent(EventType.GAME_START);
			ev.setMessage("GAME START");
			
		
			if(currP.getId() == session.getCurrentplayer().getId()){
				currP.notifyMessage(new Message("YOU'RE THE CHOOSEN ONE -> TAKE THE FIRST ROLL."));
			}else
				currP.notifyGameUpdate(ev);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private ArrayList<PlayerInfoBean> getPlayersInfo() {
		ArrayList<PlayerInfoBean> res = new ArrayList<PlayerInfoBean>();

		for (SessionPlayer p : session.getPlayers()) {
			try {
				PlayerInfoBean bean = new PlayerInfoBean(p.getId(), p.getName());
				res.add(bean);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}

		return res;
	}

}
