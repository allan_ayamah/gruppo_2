package server.notif;

import java.io.Serializable;
import java.rmi.RemoteException;

import shared.notification.BroadcastStrategy;
import shared.notification.GameEvent;
import shared.player.SessionPlayer;
import shared.service.GameSession;

public class EventBroadcastStrategy implements BroadcastStrategy, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4512174069857039662L;
	private GameEvent event;
	
	
	
	public EventBroadcastStrategy() {
		
	}
	
	public EventBroadcastStrategy(GameEvent event)  {
		super();
		this.event = event;
	}

	@Override
	public void doBroadcast(GameSession session, SessionPlayer currPlayer) throws RemoteException{
		//if(event.getSender() != null && event.getSender().getId() != currPlayer.getId())
			currPlayer.notifyGameUpdate(event);
	}
	
	public void setEvent(GameEvent ev) throws RemoteException{
		event = ev;
	}

}
