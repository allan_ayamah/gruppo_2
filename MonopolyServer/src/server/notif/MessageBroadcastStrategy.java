package server.notif;

import java.rmi.RemoteException;

import shared.Message;
import shared.notification.AbBroadcastStrategy;
import shared.player.SessionPlayer;
import shared.service.GameSession;

public class MessageBroadcastStrategy extends AbBroadcastStrategy{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1398617905488442585L;
	private Message message;
	
	public MessageBroadcastStrategy(Message mess) throws RemoteException {
		super();
		message = mess;
	}

	@Override
	public void doBroadcast(GameSession session, SessionPlayer currPlayerId) throws RemoteException {
		if(!currPlayerId.equals(message.getSender()))
			currPlayerId.notifyMessage(message);
	}

}
