package server;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;

import server.action.ActionGetCash;
import server.action.ActionGoto;
import server.game.GameSessionImpl;
import server.notif.EventBroadcastStrategy;
import server.notif.MessageBroadcastStrategy;
import shared.Message;
import shared.TurnState;
import shared.action.ActionStrategy;
import shared.action.TurnAction;
import shared.bean.Dice;
import shared.bean.PlayerInfoBean;
import shared.board.BoardConfig;
import shared.board.Space;
import shared.board.SpaceType;
import shared.notification.EventType;
import shared.notification.GameEvent;
import shared.player.AbstractSessionPlayer.PlayerStatus;
import shared.player.SessionPlayer;

public class Turn implements Serializable {

	private static final int CAUGHT_SPEEDING = 3;

	private int doublesCount = 0;
	private GameSessionImpl session;
	private SessionPlayer player;

	public TurnState state = TurnState.PRE_MOVE;

	public Turn(final SessionPlayer player, final GameSessionImpl session) {
		this.player = player;
		this.session = session;
		
		state = TurnState.PRE_MOVE;
		ArrayList<TurnAction> actions = getAvailableActions(TurnState.PRE_MOVE);
		try {
			player.setAvalableActions(actions);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void move() throws RemoteException {
		Dice diceRoll = ServerHelper.RollDice();
		int rollValue = diceRoll.getValue();
		boolean passedGo = false;

		int dest = player.getCurrentSpace() + rollValue;

		if (player.getCurrentSpace() + rollValue <= BoardConfig.MAX_SPACES - 1) {
			dest = player.getCurrentSpace() + rollValue;
		} else {
			passedGo = true;
			dest -= (BoardConfig.MAX_SPACES);
		}
		state = TurnState.POST_MOVE;
		
		ArrayList<TurnAction> actions = getAvailableActions(state);
		
		Space destSpace = session.getSpace(dest);
		if(destSpace.getType().equals(SpaceType.COMMUNITYCHEST) || destSpace.getType().equals(SpaceType.CHANCE)) 
			actions.add(TurnAction.DRAWCARD);
		if(destSpace.getType().equals(SpaceType.STREET))
			actions.add(TurnAction.PROPERTY_ACTIONS);
		

		if (passedGo) {
			try {
				actionExecuter(new ActionGetCash(200));
				player.notifyMessage(new Message("YOU PASSED GO SPACE! GET SOME MONEY"));
				session.broadcast(
						new MessageBroadcastStrategy(new Message(player.getName() + " HAS PASSED GO SPACE!!")));

			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		GameEvent ev = prepareEvent(EventType.GOTO);
		ev.setMoveVal(dest);
		ev.setDiceVale(diceRoll);
	

		if (diceRoll.isDoubles()) {
			doublesCount++;
			if (doublesCount == CAUGHT_SPEEDING) {
				actionExecuter(new ActionGoto(BoardConfig.JAIL));
				ev = prepareEvent(EventType.CAUGHT_SPEEDING);
				end();
				return;
			} else {
				state = TurnState.AGAIN;
				actions.add(TurnAction.ROLL);
				ev = prepareEventWithMessage(EventType.DOUBLES, "");
				ev.setMessage(player.getName() + " " + ev.getMessage());
				ev.setMoveVal(dest);
				ev.setDiceVale(diceRoll);
				session.broadcast(new EventBroadcastStrategy(ev));
				return;
			}
		}
		
		player.setAvalableActions(getAvailableActions(state));

		session.broadcast(new EventBroadcastStrategy(ev));
	}

	public void end() throws RemoteException {
		session.endTurn();
		session.broadcast(new MessageBroadcastStrategy(new Message(player.getName() + "'s turn is up.")));
	}

	public GameEvent prepareEvent(EventType type) {
		GameEvent ev = type.getEvent();

		try {
			ev.setSender(new PlayerInfoBean(player.getId(), player.getName()));
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ev;
	}

	public GameEvent prepareEventWithMessage(EventType type, String mess) {
		GameEvent ev = prepareEvent(type);
		if (!mess.equals(""))
			ev.setMessage(mess);
		return ev;
	}

	public void actionExecuter(ActionStrategy strategy) {
		System.out.println("Turn.actionExecuter()"+strategy.getClass());
		try {
			strategy.applyEffect(session, player.getId());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<TurnAction> getAvailableActions(TurnState st) {
		ArrayList<TurnAction> actions = new ArrayList<TurnAction>();
		if (st.equals(TurnState.PRE_MOVE)) {
			actions.add(TurnAction.ROLL);
			try {
				if (player.getStatus().equals(PlayerStatus.IN_JAIL)) {
					actions.add(TurnAction.PAY);
					actions.add(TurnAction.ROLL_FOR_FREEDOM);
					actions.add(TurnAction.USE_CARD);
				}
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else if (st.equals(TurnState.POST_MOVE)) {
			actions.add(TurnAction.END_TURN);
		}

		return actions;
	}

	public void roll() {

		if (state.equals(TurnState.PRE_MOVE) || state.equals(TurnState.AGAIN)) {

			GameEvent ev = prepareEvent(EventType.ROLL_DICE);
			try {
				session.broadcast(new EventBroadcastStrategy(ev));
				move();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}
}
