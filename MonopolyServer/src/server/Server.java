package server;

import java.rmi.Naming;
import java.rmi.RemoteException;

import server.game.GameServiceImpl;
	
public class Server {

	public static void main(String[] args) throws RemoteException {

//		BoardConfig conf = new BoardConfigImpl(new DefaultBoardBuilder());
//		System.out.println(conf.getSpaces().size());
		try {

			String serviceName = "GameService";
			GameServiceImpl gameService = new GameServiceImpl();
			
			

			Naming.rebind(serviceName, gameService);
			//Naming.rebind("BroadcastStrategy", (Remote) new EventBroadcastStrategy());

			System.out.println("GameService pronto");
		} catch (Exception e) {
			System.err.println("Server err ." + e.getMessage());
			e.printStackTrace();
		}
	}
}
