package server;

import java.rmi.RemoteException;

import shared.bean.Dice;
import shared.player.SessionPlayer;

public class ServerHelper {
	
	public static final Dice RollDice(){
		int firstRoll = (int) Math.floor(Math.random() * 5) + 1;
		int seccondRoll = (int) Math.floor(Math.random() * 5) + 1;
		return new Dice(firstRoll, seccondRoll);
	}

	public static void pay(SessionPlayer seller, SessionPlayer buyer, int amount) {
		try {
			buyer.subCash(amount);
			seller.addCash(amount);
		} catch (RemoteException e) {
			System.err.println("Transaction failed");
			e.printStackTrace();
		}
		
	}

}
